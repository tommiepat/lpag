TÍTULO I Del régimen jurídico de los actos administrativos

=======================================
CAPÍTULO I De los actos administrativos
=======================================

Artículo 1.- Concepto de acto administrativo
============================================

1.1 Son actos administrativos, las declaraciones de las entidades que, en el marco de normas de derecho público, están destinadas a producir efectos jurídicos sobre los intereses, obligaciones o derechos de los administrados dentro de una situación concreta.

1.2 No son actos administrativos:

1.2.1 Los actos de administración interna de las entidades destinados a organizar o hacer funcionar sus propias actividades o servicios. Estos actos son regulados por cada entidad, con sujeción a las disposiciones del Título Preliminar de esta Ley, y de aquellas normas que expresamente así lo establezcan.

1.2.2 Los comportamientos y actividades materiales de las entidades.

(Texto según el artículo 1 de la Ley Nº 27444)

Artículo 2.- Modalidades del acto administrativo
================================================

2.1 Cuando una ley lo autorice, la autoridad, mediante decisión expresa, puede someter el acto administrativo a condición, término o modo, siempre que dichos elementos incorporables al acto, sean compatibles con el ordenamiento legal, o cuando se trate de asegurar con ellos el cumplimiento del fin público que persigue el acto.

2.2 Una modalidad accesoria no puede ser aplicada contra el fin perseguido por el acto administrativo.

(Texto según el artículo 2 de la Ley Nº 27444)

Artículo 3.- Requisitos de validez de los actos administrativos
===============================================================

Son requisitos de validez de los actos administrativos:

1. Competencia.- Ser emitido por el órgano facultado en razón de la materia, territorio, grado, tiempo o cuantía, a través de la autoridad regularmente nominada al momento del dictado y en caso de órganos colegiados, cumpliendo los requisitos de sesión, quórum y deliberación indispensables para su emisión.

2. Objeto o contenido.- Los actos administrativos deben expresar su respectivo objeto, de tal modo que pueda determinarse inequívocamente sus efectos jurídicos. Su contenido se ajustará a lo dispuesto en el ordenamiento jurídico, debiendo ser lícito, preciso, posible física y jurídicamente, y comprender las cuestiones surgidas de la motivación.

3. Finalidad Pública.- Adecuarse a las finalidades de interés público asumidas por las normas que otorgan las facultades al órgano emisor, sin que pueda habilitársele a perseguir mediante el acto, aun encubiertamente, alguna finalidad sea personal de la propia autoridad, a favor de un tercero, u otra finalidad pública distinta a la prevista en la ley. La ausencia de normas que indique los fines de una facultad no genera discrecionalidad.

4. Motivación.- El acto administrativo debe estar debidamente motivado en proporción al contenido y conforme al ordenamiento jurídico.

5. Procedimiento regular.- Antes de su emisión, el acto debe ser conformado mediante el cumplimiento del procedimiento administrativo previsto para su generación.

(Texto según el artículo 3 de la Ley Nº 27444)

Artículo 4.- Forma de los actos administrativos
===============================================

4.1 Los actos administrativos deberán expresarse por escrito, salvo que por la naturaleza y circunstancias del caso, el ordenamiento jurídico haya previsto otra forma, siempre que permita tener constancia de su existencia.

4.2 El acto escrito indica la fecha y lugar en que es emitido, denominación del órgano del cual emana, nombre y firma de la autoridad interviniente.

4.3 Cuando el acto administrativo es producido por medio de sistemas automatizados, debe garantizarse al administrado conocer el nombre y cargo de la autoridad que lo expide.

4.4 Cuando deban emitirse varios actos administrativos de la misma naturaleza, podrá ser empleada firma mecánica o integrarse en un solo documento bajo una misma motivación, siempre que se individualice a los administrados sobre los que recae los efectos del acto. Para todos los efectos subsiguientes, los actos administrativos serán considerados como actos diferentes.

(Texto según el artículo 4 de la Ley Nº 27444)

Artículo 5.- Objeto o contenido del acto administrativo
=======================================================

5.1 El objeto o contenido del acto administrativo es aquello que decide, declara o certifica la autoridad.

5.2 En ningún caso será admisible un objeto o contenido prohibido por el orden normativo, ni incompatible con la situación de hecho prevista en las normas; ni impreciso, obscuro o imposible de realizar.

5.3 No podrá contravenir en el caso concreto disposiciones constitucionales, legales, mandatos judiciales firmes; ni podrá infringir normas administrativas de carácter general provenientes de autoridad de igual, inferior o superior jerarquía, e incluso de la misma autoridad que dicte el acto.

5.4 El contenido debe comprender todas las cuestiones de hecho y derecho planteadas por los administrados, pudiendo involucrar otras no propuestas por estos que hayan sido apreciadas de oficio, siempre que la autoridad administrativa les otorgue un plazo no menor a cinco (5) días para que expongan su posición y, en su caso, aporten las pruebas que consideren pertinentes.

(Texto modificado según el artículo 2 del Decreto Legislativo Nº 1272)

Artículo 6.- Motivación del acto administrativo
===============================================

6.1 La motivación debe ser expresa, mediante una relación concreta y directa de los hechos probados relevantes del caso específico, y la exposición de las razones jurídicas y normativas que con referencia directa a los anteriores justifican el acto adoptado.

6.2 Puede motivarse mediante la declaración de conformidad con los fundamentos y conclusiones de anteriores dictámenes, decisiones o informes obrantes en el expediente, a condición de que se les identifique de modo certero, y que por esta situación constituyan parte integrante del respectivo acto. Los informes, dictámenes o similares que sirvan de fundamento a la decisión, deben ser notificados al administrado conjuntamente con el acto administrativo.

6.3 No son admisibles como motivación, la exposición de fórmulas generales o vacías de fundamentación para el caso concreto o aquellas fórmulas que por su oscuridad, vaguedad, contradicción o insuficiencia no resulten específicamente esclarecedoras para la motivación del acto.

No constituye causal de nulidad el hecho de que el superior jerárquico de la autoridad que emitió el acto que se impugna tenga una apreciación distinta respecto de la valoración de los medios probatorios o de la aplicación o interpretación del derecho contenida en dicho acto. Dicha apreciación distinta debe conducir a estimar parcial o totalmente el recurso presentado contra el acto impugnado.

6.4 No precisan motivación los siguientes actos:

6.4.1 Las decisiones de mero trámite que impulsan el procedimiento.

6.4.2 Cuando la autoridad estima procedente lo pedido por el administrado y el acto administrativo no perjudica derechos de terceros.

6.4.3 Cuando la autoridad produce gran cantidad de actos administrativos sustancialmente iguales, bastando la motivación única.

(Texto modificado según el artículo 2 del Decreto Legislativo Nº 1272)

Artículo 7.- Régimen de los actos de administración interna
===========================================================

7.1 Los actos de administración interna se orientan a la eficacia y eficiencia de los servicios y a los fines permanentes de las entidades. Son emitidos por el órgano competente, su objeto debe ser física y jurídicamente posible, su motivación es facultativa cuando los superiores jerárquicos impartan las órdenes a sus subalternos en la forma legalmente prevista.

El régimen de eficacia anticipada de los actos administrativos previsto en el artículo 17 es susceptible de ser aplicado a los actos de administración interna, siempre que no se violen normas de orden público ni afecte a terceros.

7.2 Las decisiones internas de mero trámite, pueden impartirse verbalmente por el órgano competente, en cuyo caso el órgano inferior que las reciba las documentará por escrito y comunicará de inmediato, indicando la autoridad de quien procede mediante la fórmula, “Por orden de ...”

(Texto modificado según el artículo 2 del Decreto Legislativo Nº 1272)

