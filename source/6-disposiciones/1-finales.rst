=====================================
DISPOSICIONES COMPLEMENTARIAS FINALES
=====================================

Primera.- Referencias a esta Ley
================================

Las referencias a las normas de la presente Ley se efectuarán indicando el número del artículo seguido de la mención “de la Ley del Procedimiento Administrativo General”.

(Texto según la sección de las Disposiciones Complementarias y Finales de la Ley Nº 27444)

Segunda.- Prohibición de reiterar contenidos normativos
=======================================================

Las disposiciones legales posteriores no pueden reiterar el contenido de las normas de la presente Ley, debiendo sólo referirse al artículo respectivo o concretarse a regular aquello no previsto.

(Texto según la sección de las Disposiciones Complementarias y Finales de la Ley Nº 27444)

Tercera.- Vigencia de la presente Ley
=====================================

1. Esta Ley entrará en vigor a los seis meses de su publicación en el Diario Oficial El Peruano. (*) NOTA SPIJ

2. La falta de reglamentación de alguna de las disposiciones de esta Ley no será impedimento para su vigencia y exigibilidad.

(Texto según la sección de las Disposiciones Complementarias y Finales de la Ley Nº 27444)

Cuarta
======

Cuarta.- Las ordenanzas expedidas por las Municipalidades Distritales que aprueban el monto de los derechos de tramitación de los procedimientos contenidos en su Texto Único de Procedimientos Administrativos que deben ser materia de ratificación por parte de las Municipalidades Provinciales de su circunscripción según lo establecido en el artículo 40 de la Ley Nº 27972- Ley Orgánica de Municipalidades, deben ser ratificadas en un plazo máximo de cuarenta y cinco (45) días hábiles, salvo las tasas por arbitrios en cuyo caso el plazo es de sesenta (60) días hábiles.

(Texto modificado según la Única Disposición Complementaria Modificatoria del Decreto Legislativo Nº 1452)

La ordenanza se considera ratificada si, vencido el plazo establecido como máximo para pronunciarse la Municipalidad Provincial no hubiera emitido la ratificación correspondiente, no siendo necesario pronunciamiento expreso adicional.

La vigencia de la ordenanza así ratificada, requiere su publicación en el diario oficial El Peruano o en el diario encargado de los avisos judiciales en la capital del departamento o provincia, por parte de la municipalidad distrital respectiva.

La ratificación a que se refiere la presente disposición no es de aplicación a los derechos de tramitación de los procedimientos administrativos estandarizados obligatorios aprobados por la Presidencia del Consejo de Ministros.

(Texto según la sección de las Disposiciones Complementarias Finales del Decreto Legislativo Nº 1272)

Quinta
======

Quinta.- Las competencias otorgadas a la Presidencia del Consejo de Ministros por medio del artículo 48 de la Ley Nº 27444, Ley del Procedimiento Administrativo General, son también aplicables al Sistema Único de Trámites (SUT) para la simplificación de procedimientos y servicios prestados en exclusividad, creado por Decreto Legislativo Nº 1203.

(Texto según la sección de las Disposiciones Complementarias Finales del Decreto Legislativo Nº 1272)

Sexta.- Aprobación de Textos Únicos Ordenados
=============================================

Las entidades del Poder Ejecutivo se encuentran facultadas a compilar en el respectivo Texto Único Ordenado las modificaciones efectuadas a disposiciones legales o reglamentarias de alcance general correspondientes al sector al que pertenecen con la finalidad de compilar toda la normativa en un solo texto.

Su aprobación se produce mediante decreto supremo del sector correspondiente, debiendo contar con la opinión previa favorable del Ministerio de Justicia y Derechos Humanos.

(Texto según la sección de las Disposiciones Complementarias Finales del Decreto Legislativo Nº 1452)

Sétima.- Elaboración de Guía para la elaboración de proyectos de normas reglamentarias
======================================================================================

El Ministerio de Justicia y Derechos Humanos, en un plazo no mayor a 120 (ciento veinte) días hábiles de publicado el presente Decreto Legislativo, emite una Guía para la elaboración de proyectos de normas reglamentarias, de obligatorio cumplimiento para todas las entidades de la Administración Pública.

(Texto según la sección de las Disposiciones Complementarias Finales del Decreto Legislativo Nº 1452)

Octava.- Adecuación del Texto Único Ordenado de la Ley Nº 27444, Ley del Procedimiento Administrativo General
=============================================================================================================

El Ministerio de Justicia y Derechos Humanos, en un plazo no mayor a 60 (sesenta) días hábiles de publicado el presente Decreto Legislativo, incorpora las modificaciones contenidas en la presente norma al Texto Único Ordenado de la Ley del Procedimiento Administrativo General, aprobado por Decreto Supremo Nº 006-2017-JUS.

(Texto según la sección de las Disposiciones Complementarias Finales del Decreto Legislativo Nº 1452)

Novena.- Fundamentación del silencio administrativo negativo
============================================================

La obligación de fundamentar en una disposición sustantiva la calificación del silencio administrativo negativo en un procedimiento administrativo prevista en el numeral 34.1 de la Ley Nº 27444, Ley del Procedimiento Administrativo General, resulta aplicable para las regulaciones que se aprueben a partir de la entrada en vigencia del presente Decreto Legislativo.

(Texto según la sección de las Disposiciones Complementarias Finales del Decreto Legislativo Nº 1452)

Décima.- Proceso de tránsito
============================

Al 31 de diciembre del 2018 culmina la transferencia de la Presidencia del Consejo de Ministros a la entidad competente, del acervo documentario e instrumentos relacionados a la Metodología para la determinación de los derechos de tramitación de los procedimientos administrativos y servicios prestados en exclusividad.

(Texto según la sección de las Disposiciones Complementarias Finales del Decreto Legislativo Nº 1452)

