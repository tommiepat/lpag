TÍTULO V De la responsabilidad de la administración pública y del personal a su servicio

=======================================================
CAPÍTULO I Responsabilidad de la administración pública
=======================================================

Artículo 260.- Disposiciones Generales
======================================

260.1 Sin perjuicio de las responsabilidades previstas en el derecho común y en las leyes especiales, las entidades son patrimonialmente responsables frente a los administrados por los daños directos e inmediatos causados por los actos de la administración o los servicios públicos directamente prestados por aquéllas.

260.2 En los casos del numeral anterior, no hay lugar a la reparación por parte de la Administración, cuando el daño fuera consecuencia de caso fortuito o fuerza mayor, de hecho determinante del administrado damnificado o de tercero.

Tampoco hay lugar a reparación cuando la entidad hubiere actuado razonable y proporcionalmente en defensa de la vida, integridad o los bienes de las personas o en salvaguarda de los bienes públicos o cuando se trate de daños que el administrado tiene el deber jurídico de soportar de acuerdo con el ordenamiento jurídico y las circunstancias.

260.3 La declaratoria de nulidad de un acto administrativo en sede administrativa o por resolución judicial no presupone necesariamente derecho a la indemnización.

260.4 EI daño alegado debe ser efectivo, valuable económicamente e individualizado con relación a un administrado o grupo de ellos.

260.5 La indemnización comprende el daño directo e inmediato y las demás consecuencias que se deriven de la acción u omisión generadora del daño, incluyendo el lucro cesante, el daño a la persona y el daño moral.

260.6 Cuando la entidad indemnice a los administrados, podrá repetir judicialmente de autoridades y demás personal a su servicio la responsabilidad en que hubieran incurrido, tomando en cuenta la existencia o no de intencionalidad, la responsabilidad profesional del personal involucrado y su relación con la producción del perjuicio. Sin embargo, la entidad podrá acordar con el responsable el reembolso de lo indemnizado, aprobando dicho acuerdo mediante resolución.

(Texto según el artículo 238 de la Ley Nº 27444)

