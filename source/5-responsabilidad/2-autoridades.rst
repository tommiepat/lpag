==================================================================================================
CAPÍTULO II Responsabilidad de las autoridades y personal al servicio de la administración pública
==================================================================================================

Artículo 261.- Faltas administrativas
=====================================

261.1 Las autoridades y personal al servicio de las entidades, independientemente de su régimen laboral o contractual, incurren en falta administrativa en el trámite de los procedimientos administrativos a su cargo y, por ende, son susceptibles de ser sancionados administrativamente suspensión, cese o destitución atendiendo a la gravedad de la falta, la reincidencia, el daño causado y la intencionalidad con que hayan actuado, en caso de:

1. Negarse a recibir injustificadamente solicitudes, recursos, declaraciones, informaciones o expedir constancia sobre ellas.

2. No entregar, dentro del término legal, los documentos recibidos a la autoridad que deba decidir u opinar sobre ellos.

3. Demorar injustificadamente la remisión de datos, actuados o expedientes solicitados para resolver un procedimiento o la producción de un acto procesal sujeto a plazo determinado dentro del procedimiento administrativo.

4. Resolver sin motivación algún asunto sometido a su competencia.

5. Ejecutar un acto que no se encuentre expedito para ello.

6. No comunicar dentro del término legal la causal de abstención en la cual se encuentra incurso.

7. Dilatar el cumplimiento de mandatos superiores o administrativo o contradecir sus decisiones.

8. Intimidar de alguna manera a quien desee plantear queja administrativa o contradecir sus decisiones.

9. Incurrir en ilegalidad manifiesta.

10. Difundir de cualquier modo o permitir el acceso a la información confidencial a que se refiere el numeral 171.1 de este TUO (*) RECTIFICADO POR FE DE ERRATAS

11. No resolver dentro del plazo establecido para cada procedimiento administrativo de manera negligente o injustificada.

12. Desconocer de cualquier modo la aplicación de la aprobación automática o silencio positivo obtenido por el administrado ante la propia u otra entidad administrativa.

13. Incumplir con los criterios, procedimientos y metodologías para la determinación de los costos de los procedimientos y servicios administrativos.

14. Cobrar montos de derecho de tramitación por encima de una (1) UIT, sin contar con autorización previa.

15. No aplicar el procedimiento estandarizado aprobado.

16. Cobrar montos de derecho de tramitación superiores al establecido para los procedimientos estandarizados.

17. Proponer, aprobar o exigir procedimientos, requisitos o tasas en contravención a los dispuestos en esta ley y demás normas de simplificación, aunque consten en normas internas de las entidades o Texto Único de Procedimientos Administrativos.

18. Exigir a los administrados la presentación de documentos prohibidos de solicitar o no admitir los sucedáneos documentales considerados en la presente ley, aun cuando su exigencia se base en alguna norma interna de la entidad o en su Texto Único de Procedimientos Administrativos.

19. Suspender la admisión a trámite de solicitudes de los administrados por cualquier razón.

20. Negarse a recibir los escritos, declaraciones o formularios presentados por los administrados, o a expedir constancia de su recepción, lo que no impide que pueda formular las observaciones en los términos a que se refiere el artículo 136.

21. Exigir la presentación personal de peticiones, recursos o documentos cuando la normativa no lo exija.

22. Otros incumplimientos que sean tipificados por Decreto Supremo refrendado por Presidencia del Consejo de Ministros.

261.2 Las correspondientes sanciones deben ser impuestas previo proceso administrativo disciplinario que, se ceñirá a las disposiciones legales vigentes sobre la materia, debiendo aplicarse para los demás casos el procedimiento establecido en el artículo 255, en lo que fuere pertinente.

(Texto según el artículo 239 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

Artículo 262.- Restricciones a ex autoridades de las entidades
==============================================================

262.1 Ninguna ex autoridad de las entidades podrá realizar durante el año siguiente a su cese alguna de las siguientes acciones con respecto a la entidad a la cual perteneció:

262.1.1 Representar o asistir a un administrado en algún procedimiento respecto del cual tuvo algún grado de participación durante su actividad en la entidad.

262.1.2 Asesorar a cualquier administrado en algún asunto que estaba pendiente de decisión durante su relación con la entidad.

262.1.3 Realizar cualquier contrato, de modo directo o indirecto, con algún administrado apersonado a un procedimiento resuelto con su participación.

262.2 La transgresión a estas restricciones será objeto de procedimiento investigatorio y, de comprobarse, el responsable será sancionado con la prohibición de ingresar a cualquier entidad por cinco años, e inscrita en el Registro respectivo.

(Texto según el artículo 241 de la Ley Nº 27444)

Artículo 263.- Registro Nacional de Sanciones contra Servidores Civiles
=======================================================================

El Registro Nacional de Sanciones contra Servidores Civiles consolida toda la información relativa al ejercicio de la potestad administrativa sancionadora disciplinaria y funcional ejercida por las entidades de la Administración Pública, así como aquellas sanciones penales impuestas de conformidad con los artículos 296, 296-A primer, segundo y cuarto párrafo; 296-B, 297, 382, 383, 384, 387, 388, 389, 393, 393-A, 394, 395, 396, 397, 397-A, 398, 399, 400 y 401 del Código Penal, así como el artículo 4-A del Decreto Ley 25475 y los delitos previstos en los artículos 1, 2 y 3 del Decreto Legislativo 1106.

(Artículo modificado por el artículo 2 del Decreto Legislativo Nº 1367)

Artículo 264.- Autonomía de responsabilidades
=============================================

264.1 Las consecuencias civiles, administrativas o penales de la responsabilidad de las autoridades son independientes y se exigen de acuerdo a lo previsto en su respectiva legislación.

264.2 Los procedimientos para la exigencia de la responsabilidad penal o civil no afectan la potestad de las entidades para instruir y decidir sobre la responsabilidad administrativa, salvo disposición judicial expresa en contrario.

(Texto según el artículo 243 de la Ley Nº 27444)

Artículo 265.- Denuncia por delito de omisión o retardo de función
==================================================================

El Ministerio Público, a efectos de decidir el ejercicio de la acción penal en los casos referidos a delitos de omisión o retardo de función, deberá determinar la presencia de las siguientes situaciones:

a) Si el plazo previsto por ley para que el funcionario actúe o se pronuncie de manera expresa no ha sido excedido.

b) Si el administrado ha consentido de manera expresa en lo resuelto por el funcionario público.

(Texto según el artículo 244 de la Ley Nº 27444)

