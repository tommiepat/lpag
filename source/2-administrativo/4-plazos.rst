=============================
CAPÍTULO IV Plazos y Términos
=============================

(Texto según el artículo 130 de la Ley Nº 27444)

Artículo 142.- Obligatoriedad de plazos y términos
==================================================

142.1 Los plazos y términos son entendidos como máximos, se computan independientemente de cualquier formalidad, y obligan por igual a la administración y a los administrados, sin necesidad de apremio, en aquello que respectivamente les concierna. Los plazos para el pronunciamiento de las entidades, en los procedimientos administrativos, se contabilizan a partir del día siguiente de la fecha en la cual el administrado presentó su solicitud, salvo que se haya requerido subsanación en cuyo caso se contabilizan una vez efectuada esta.

142.2 Toda autoridad debe cumplir con los términos y plazos a su cargo, así como supervisar que los subalternos cumplan con los propios de su nivel.

142.3 Es derecho de los administrados exigir el cumplimiento de los plazos y términos establecidos para cada actuación o servicio.

(Texto según el artículo 131 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

Artículo 143.- Plazos máximos para realizar actos procedimentales
=================================================================

A falta de plazo establecido por ley expresa, las actuaciones deben producirse dentro de los siguientes:

1. Para recepción y derivación de un escrito a la unidad competente: dentro del mismo día de su presentación.

2. Para actos de mero trámite y decidir peticiones de ese carácter: en tres días.

3. Para emisión de dictámenes, peritajes, informes y similares: dentro de siete días después de solicitados; pudiendo ser prorrogado a tres días más si la diligencia requiere el traslado fuera de su sede o la asistencia de terceros.

4. Para actos de cargo del administrado requeridos por la autoridad, como entrega de información, respuesta a las cuestiones sobre las cuales deban pronunciarse: dentro de los diez días de solicitados.

(Texto según el artículo 132 de la Ley Nº 27444)

Artículo 144.- Inicio de cómputo
================================

144.1 El plazo expresado en días es contado a partir del día hábil siguiente de aquel en que se practique la notificación o la publicación del acto, salvo que éste señale una fecha posterior, o que sea necesario efectuar publicaciones sucesivas, en cuyo caso el cómputo es iniciado a partir de la última.

144.2 El plazo expresado en meses o años es contado a partir de la notificación o de la publicación del respectivo acto, salvo que éste disponga fecha posterior.

(Texto según el artículo 133 de la Ley Nº 27444)

Artículo 145.- Transcurso del plazo
===================================

145.1 Cuando el plazo es señalado por días, se entenderá por hábiles consecutivos, excluyendo del cómputo aquellos no laborables del servicio, y los feriados no laborables de orden nacional o regional.

145.2 Cuando el último día del plazo o la fecha determinada es inhábil o por cualquier otra circunstancia la atención al público ese día no funcione durante el horario normal, son entendidos prorrogados al primer día hábil siguiente.

145.3 Cuando el plazo es fijado en meses o años, es contado de fecha a fecha, concluyendo el día igual al del mes o año que inició, completando el número de meses o años fijados para el lapso. Si en el mes de vencimiento no hubiere día igual a aquel en que comenzó el cómputo, es entendido que el plazo expira el primer día hábil del siguiente mes calendario.

(Texto según el artículo 134 de la Ley Nº 27444)

Artículo 146.- Término de la distancia
======================================

146.1 Al cómputo de los plazos establecidos en el procedimiento administrativo, se agrega el término de la distancia previsto entre el lugar de domicilio del administrado dentro del territorio nacional y el lugar de la unidad de recepción más cercana a aquél facultado para llevar a cabo la respectiva actuación.

146.2 El cuadro de términos de la distancia es aprobado por la autoridad competente.

En caso que el titular de la entidad no haya aprobado el cuadro de términos de la distancia correspondiente, debe aplicar el régimen establecido en el Cuadro General de Términos de la Distancia aprobado por el Poder Judicial.

(Texto según el artículo 135 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

Artículo 147. Plazos improrrogables
===================================

147.1 Los plazos fijados por norma expresa son improrrogables, salvo disposición habilitante en contrario.

147.2 La autoridad competente puede otorgar prórroga a los plazos establecidos para la actuación de pruebas o para la emisión de informes o dictámenes, cuando así lo soliciten antes de su vencimiento los administrados o los funcionarios, respectivamente.

147.3 La prórroga es concedida por única vez mediante decisión expresa, siempre que el plazo no haya sido perjudicado por causa imputable a quien la solicita y siempre que aquella no afecte derechos de terceros.

147.4 Tratándose de procedimientos iniciados a pedido de parte con aplicación del silencio administrativo positivo, en caso el administrado deba realizar una gestión de trámite a su cargo necesaria para adoptar una decisión de fondo, puede solicitar la suspensión del cómputo del plazo del procedimiento hasta por un plazo de treinta (30) días hábiles.

(Texto según el artículo 136 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

Artículo 148.- Régimen para días inhábiles
==========================================

148.1 El Poder Ejecutivo fija por decreto supremo, dentro del ámbito geográfico nacional u alguno particular, los días inhábiles, a efecto del cómputo de plazos administrativos.

148.2 Esta norma debe publicarse previamente y difundirse permanentemente en los ambientes de las entidades, a fin de permitir su conocimiento a los administrados.

148.3 Las entidades no pueden unilateralmente inhabilitar días, y, aun en caso de fuerza mayor que impida el normal funcionamiento de sus servicios, debe garantizar el mantenimiento del servicio de su unidad de recepción documental.

(Texto según el artículo 137 de la Ley Nº 27444)

Artículo 149. Régimen de las horas hábiles
==========================================

El horario de atención de las entidades para la realización de cualquier actuación se rige por las siguientes reglas:

1. Son horas hábiles las correspondientes al horario fijado para el funcionamiento de la entidad, sin que en ningún caso la atención a los usuarios pueda ser inferior a ocho horas diarias consecutivas.

2. El horario de atención diario es establecido por cada entidad cumpliendo un período no coincidente con la jornada laboral ordinaria, para favorecer el cumplimiento de las obligaciones y actuaciones de la ciudadanía. Para el efecto, distribuye su personal en turnos, cumpliendo jornadas no mayores de ocho horas diarias.

3. El horario de atención es continuado para brindar sus servicios a todos los asuntos de su competencia, sin fraccionarlo para atender algunos en determinados días u horas, ni afectar su desarrollo por razones personales.

4. El horario de atención concluye con la prestación del servicio a la última persona compareciente dentro del horario hábil.

5. Los actos de naturaleza continua iniciados en hora hábil son concluidos sin afectar su validez después del horario de atención, salvo que el administrado consienta en diferirlos. Dicho consentimiento debe constar de forma indubitable.

6. En cada servicio rige la hora seguida por la entidad; en caso de duda o a falta de aquella, debe verificarse en el acto, si fuere posible, la hora oficial, que prevalecerá.

(Texto según el artículo 138 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

Artículo 150.- Cómputo de días calendario
=========================================

150.1 Tratándose del plazo para el cumplimiento de actos procedimentales internos a cargo de las entidades, la norma legal puede establecer que su cómputo sea en días calendario, o que el término expire con la conclusión del último día aun cuando fuera inhábil.

150.2 Cuando una ley señale que el cómputo del plazo para un acto procedimental a cargo del administrado sea en días calendario, esta circunstancia le es advertida expresamente en la notificación.

(Texto según el artículo 139 de la Ley Nº 27444)

Artículo 151.- Efectos del vencimiento del plazo
================================================

151.1 El plazo vence el último momento del día hábil fijado, o anticipadamente, si antes de esa fecha son cumplidas las actuaciones para las que fuera establecido.

151.2 Al vencimiento de un plazo improrrogable para realizar una actuación o ejercer una facultad procesal, previo apercibimiento, la entidad declara decaído el derecho al correspondiente acto, notificando la decisión.

151.3 El vencimiento del plazo para cumplir un acto a cargo de la Administración, no exime de sus obligaciones establecidas atendiendo al orden público. La actuación administrativa fuera de término no queda afecta de nulidad, salvo que la ley expresamente así lo disponga por la naturaleza perentoria del plazo.

151.4 La preclusión por el vencimiento de plazos administrativos opera en procedimientos trilaterales, concurrenciales, y en aquellos que por existir dos o más administrados con intereses divergentes, deba asegurárselas tratamiento paritario.

(Texto según el artículo 140 de la Ley Nº 27444)

Artículo 152.- Adelantamiento de plazos
=======================================

La autoridad a cargo de la instrucción del procedimiento mediante decisión irrecurrible, puede reducir los plazos o anticipar los términos, dirigidos a la administración, atendiendo razones de oportunidad o conveniencia del caso.

(Texto según el artículo 141 de la Ley Nº 27444)

Artículo 153.- Plazo máximo del procedimiento administrativo
============================================================

No puede exceder de treinta días el plazo que transcurra desde que es iniciado un procedimiento administrativo de evaluación previa hasta aquel en que sea dictada la resolución respectiva, salvo que la ley establezca trámites cuyo cumplimiento requiera una duración mayor.

(Texto según el artículo 142 de la Ley Nº 27444)

Artículo 154.- Responsabilidad por incumplimiento de plazos
===========================================================

154.1 El incumplimiento injustificado de los plazos previstos para las actuaciones de las entidades genera responsabilidad disciplinaria para la autoridad obligada, sin perjuicio de la responsabilidad civil por los daños y perjuicios que pudiera haber ocasionado.

154.2 También alcanza solidariamente la responsabilidad al superior jerárquico, por omisión en la supervisión, si el incumplimiento fuera reiterativo o sistemático.

(Texto según el artículo 143 de la Ley Nº 27444)

