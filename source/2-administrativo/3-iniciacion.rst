=========================================
CAPÍTULO III Iniciación del procedimiento
=========================================

Artículo 114.- Formas de iniciación del procedimiento
=====================================================

El procedimiento administrativo es promovido de oficio por el órgano competente o instancia del administrado, salvo que por disposición legal o por su finalidad corresponda ser iniciado exclusivamente de oficio o a instancia del interesado.

(Texto según el artículo 103 de la Ley Nº 27444)

Artículo 115.- Inicio de oficio
===============================

115.1 Para el inicio de oficio de un procedimiento debe existir disposición de autoridad superior que la fundamente en ese sentido, una motivación basada en el cumplimiento de un deber legal o el mérito de una denuncia.

115.2 El inicio de oficio del procedimiento es notificado a los administrados determinados cuyos intereses o derechos protegidos puedan ser afectados por los actos a ejecutar, salvo en caso de fiscalización posterior a solicitudes o a su documentación, acogidos a la presunción de veracidad. La notificación incluye la información sobre la naturaleza, alcance y de ser previsible, el plazo estimado de su duración, así como de sus derechos y obligaciones en el curso de tal actuación.

115.3 La notificación es realizada inmediatamente luego de emitida la decisión, salvo que la normativa autorice que sea diferida por su naturaleza confidencial basada en el interés público.

(Texto según el artículo 104 de la Ley Nº 27444)

Artículo 116.- Derecho a formular denuncias
===========================================

116.1 Todo administrado está facultado para comunicar a la autoridad competente aquellos hechos que conociera contrarios al ordenamiento, sin necesidad de sustentar la afectación inmediata de algún derecho o interés legítimo, ni que por esta actuación sea considerado sujeto del procedimiento.

116.2 La comunicación debe exponer claramente la relación de los hechos, las circunstancias de tiempo, lugar y modo que permitan su constatación, la indicación de sus presuntos autores, partícipes y damnificados, el aporte de la evidencia o su descripción para que la administración proceda a su ubicación, así como cualquier otro elemento que permita su comprobación.

116.3 Su presentación obliga a practicar las diligencias preliminares necesarias y, una vez comprobada su verosimilitud, a iniciar de oficio la respectiva fiscalización. El rechazo de una denuncia debe ser motivado y comunicado al denunciante, si estuviese individualizado.

116.4 La entidad receptora de la denuncia puede otorgar medidas de protección al denunciante, garantizando su seguridad y evitando se le afecte de algún modo.

(Texto según el artículo 105 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

Artículo 117.- Derecho de petición administrativa
=================================================

117.1 Cualquier administrado, individual o colectivamente, puede promover por escrito el inicio de un procedimiento administrativo ante todas y cualesquiera de las entidades, ejerciendo el derecho de petición reconocido en el artículo 2 inciso 20) de la Constitución Política del Estado.

117.2 El derecho de petición administrativa comprende las facultades de presentar solicitudes en interés particular del administrado, de realizar solicitudes en interés general de la colectividad, de contradecir actos administrativos, las facultades de pedir informaciones, de formular consultas y de presentar solicitudes de gracia.

117.3 Este derecho implica la obligación de dar al interesado una respuesta por escrito dentro del plazo legal.

(Texto según el artículo 106 de la Ley Nº 27444)

Artículo 118.- Solicitud en interés particular del administrado
===============================================================

Cualquier administrado con capacidad jurídica tiene derecho a presentarse personalmente o hacerse representar ante la autoridad administrativa, para solicitar por escrito la satisfacción de su interés legítimo, obtener la declaración, el reconocimiento u otorgamiento de un derecho, la constancia de un hecho, ejercer una facultad o formular legítima oposición.

(Texto según el artículo 107 de la Ley Nº 27444)

Artículo 119.- Solicitud en interés general de la colectividad
==============================================================

119.1 Las personas naturales o jurídicas pueden presentar petición o contradecir actos ante la autoridad administrativa competente, aduciendo el interés difuso de la sociedad.

119.2 Comprende esta facultad la posibilidad de comunicar y obtener respuesta sobre la existencia de problemas, trabas u obstáculos normativos o provenientes de prácticas administrativas que afecten el acceso a las entidades, la relación con administrados o el cumplimiento de los principios procedimentales, así como a presentar alguna sugerencia o iniciativa dirigida a mejorar la calidad de los servicios, incrementar el rendimiento o cualquier otra medida que suponga un mejor nivel de satisfacción de la sociedad respecto a los servicios públicos.

(Texto según el artículo 108 de la Ley Nº 27444)

Artículo 120.- Facultad de contradicción administrativa
=======================================================

120.1 Frente a un acto que supone que viola, afecta, desconoce o lesiona un derecho o un interés legítimo, procede su contradicción en la vía administrativa en la forma prevista en esta Ley, para que sea revocado, modificado, anulado o sean suspendidos sus efectos.

120.2 Para que el interés pueda justificar la titularidad del administrado, debe ser legítimo, personal, actual y probado. El interés puede ser material o moral.

120.3 La recepción o atención de una contradicción no puede ser condicionada al previo cumplimiento del acto respectivo.

(Texto según el artículo 109 de la Ley Nº 27444)

Artículo 121.- Facultad de solicitar información
================================================

121.1 El derecho de petición incluye el de solicitar la información que obra en poder de las entidades, siguiendo el régimen previsto en la Constitución y la Ley.

121.2 Las entidades establecen mecanismos de atención a los pedidos sobre información específica y prevén el suministro de oficio a los interesados, incluso vía telefónica o por medios electrónicos, de la información general sobre los temas de interés recurrente para la ciudadanía.

121.3 Las entidades están obligadas a responder la solicitud de información dentro del plazo legal.

(Texto según el artículo 110 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

Artículo 122.- Facultad de formular consultas
=============================================

122.1 El derecho de petición incluye las consultas por escrito a las autoridades administrativas, sobre las materias a su cargo y el sentido de la normativa vigente que comprende su accionar, particularmente aquella emitida por la propia entidad. Este derecho implica la obligación de dar al interesado una respuesta por escrito dentro del plazo legal.

122.2 Cada entidad atribuye a una o más de sus unidades competencia para absolver las consultas sobre la base de los precedentes de interpretación seguidos en ella.

(Texto según el artículo 111 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

Artículo 123.- Facultad de formular peticiones de gracia
========================================================

123.1 Por la facultad de formular peticiones de gracia, el administrado puede solicitar al titular de la entidad competente la emisión de un acto sujeto a su discrecionalidad o a su libre apreciación, o prestación de un servicio cuando no cuenta con otro título legal específico que permita exigirlo como una petición en interés particular.

123.2 Frente a esta petición, la autoridad comunica al administrado la calidad graciable de lo solicitado y es atendido directamente mediante la prestación efectiva de lo pedido, salvo disposición expresa de la ley que prevea una decisión formal para su aceptación.

123.3 Este derecho se agota con su ejercicio en la vía administrativa, sin perjuicio del ejercicio de otros derechos reconocidos por la Constitución.

(Texto según el artículo 112 de la Ley Nº 27444)

Artículo 124.- Requisitos de los escritos
=========================================

Todo escrito que se presente ante cualquier entidad debe contener lo siguiente:

1. Nombres y apellidos completos, domicilio y número de Documento Nacional de Identidad o carné de extranjería del administrado, y en su caso, la calidad de representante y de la persona a quien represente.

2. La expresión concreta de lo pedido, los fundamentos de hecho que lo apoye y, cuando le sea posible, los de derecho.

3. Lugar, fecha, firma o huella digital, en caso de no saber firmar o estar impedido.

4. La indicación del órgano, la entidad o la autoridad a la cual es dirigida, entendiéndose por tal, en lo posible, a la autoridad de grado más cercano al usuario, según la jerarquía, con competencia para conocerlo y resolverlo.

5. La dirección del lugar donde se desea recibir las notificaciones del procedimiento, cuando sea diferente al domicilio real expuesto en virtud del numeral 1. Este señalamiento de domicilio surte sus efectos desde su indicación y es presumido subsistente, mientras no sea comunicado expresamente su cambio.

6. La relación de los documentos y anexos que acompaña, indicados en el TUPA.

7. La identificación del expediente de la materia, tratándose de procedimientos ya iniciados.

(Texto según el artículo 113 de la Ley Nº 27444)

Artículo 125.- Copias de escritos
=================================

125.1 El escrito es presentado en papel simple acompañado de una copia conforme y legible, salvo que fuere necesario un número mayor para notificar a terceros. La copia es devuelta al administrado con la firma de la autoridad y el sello de recepción que indique fecha, hora y lugar de presentación.

125.2 El cargo así expedido tiene el mismo valor legal que el original.

(Texto según el artículo 114 de la Ley Nº 27444)

Artículo 126.- Representación del administrado
==============================================

126.1 Para la tramitación de los procedimientos, es suficiente carta poder simple con firma del administrado, salvo que leyes especiales requieran una formalidad adicional.

126.2 Para el desistimiento de la pretensión o del procedimiento, acogerse a las formas de terminación convencional del procedimiento o, para el cobro de dinero, es requerido poder especial indicando expresamente el o los actos para los cuales fue conferido. El poder especial es formalizado a elección del administrado, mediante documento privado con firmas legalizadas ante notario o funcionario público autorizado para el efecto, así como mediante declaración en comparecencia personal del administrado y representante ante la autoridad.

126.3 El empleo de la representación no impide la intervención del propio administrado cuando lo considere pertinente, ni el cumplimiento por éste de las obligaciones que exijan su comparecencia personal según las normas de la presente Ley.

(Texto según el artículo 115 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

Artículo 127.- Acumulación de solicitudes
=========================================

127.1 En caso de ser varios los administrados interesados en obtener un mismo acto administrativo sin intereses incompatibles, pueden comparecer conjuntamente por medio de un solo escrito, conformando un único expediente.

127.2 Pueden acumularse en un solo escrito más de una petición siempre que se trate de asuntos conexos que permitan tramitarse y resolverse conjuntamente, pero no planteamientos subsidiarios o alternativos, salvo lo establecido en el numeral 217.4 del artículo 217.

127.3 Si a criterio de la autoridad administrativa no existiera conexión o existiera incompatibilidad entre las peticiones planteadas en un escrito, se les emplazará para que presente peticiones por separado, bajo apercibimiento de proceder de oficio a sustanciarlas individualmente si fueren separables, o en su defecto disponer el abandono del procedimiento.

(Texto según el artículo 116 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

Artículo 128.- Recepción documental
===================================

128.1 Cada entidad tiene su unidad general de recepción documental, trámite documentado o mesa de partes, salvo cuando la entidad brinde servicios en varios inmuebles ubicados en zonas distintas, en cuyo caso corresponde abrir en cada local registros auxiliares al principal, al cual reportan todo registro que realicen.

128.2 Tales unidades están a cargo de llevar un registro del ingreso de los escritos que sean presentados y la salida de aquellos documentos emitidos por la entidad dirigidos a otros órganos o administrados. Para el efecto, expiden el cargo, practican los asientos respectivos respetando su orden de ingreso o salida, indicando su número de ingreso, naturaleza, fecha, remitente y destinatario. Concluido el registro, los escritos o resoluciones deben ser cursados el mismo día a sus destinatarios.

128.3 Dichas unidades tenderán a administrar su información en soporte informático, cautelando su integración a un sistema único de trámite documentado.

128.4 También a través de dichas unidades los administrados realizan todas las gestiones pertinentes a sus procedimientos y obtienen la información que requieran con dicha finalidad.

(Texto según el artículo 117 de la Ley Nº 27444)

Artículo 129.- Reglas para celeridad en la recepción
====================================================

Las entidades adoptan las siguientes acciones para facilitar la recepción personal de los escritos de los administrados y evitar su aglomeración:

1. La puesta en vigencia de programas de racionalización del tiempo de atención por usuario y la mayor provisión simultánea de servidores dedicados exclusivamente a la atención de los usuarios.

2. El servicio de asesoramiento a los usuarios para completar formularios o modelo de documentos.

3. Adecuar su régimen de horas hábiles para la atención al público, a fin de adaptarlo a las formas previstas en el artículo 149.

4. Estudiar la estacionalidad de la demanda de sus servicios y dictar las medidas preventivas para evitarla.

5. Instalar mecanismos de autoservicio que permita a los usuarios suministrar directamente su información, tendiendo al empleo de niveles avanzados de digitalización.

(Texto según el artículo 118 de la Ley Nº 27444)

Artículo 130.- Reglas generales para la recepción documental
============================================================

Los escritos que los administrados dirigen a las entidades pueden ser presentados de modo personal o a través de terceros, ante las unidades de recepción de:

1. Los órganos administrativos a los cuales van dirigidos.

2. Los órganos desconcentrados de la entidad.

3. Las autoridades políticas del Ministerio del Interior en la circunscripción correspondiente.

4. En las oficinas de correo, en la manera expresamente prevista en esta Ley.

5. En las representaciones diplomáticas u oficinas consulares en el extranjero, tratándose de administrados residentes en el exterior, quienes derivan los escritos a la entidad competente, con indicación de la fecha de su presentación.

(Texto según el artículo 119 de la Ley Nº 27444)

Artículo 131.- Presentación mediante correo certificado
=======================================================

131.1 Los administrados pueden remitir sus escritos, con recaudos completos, mediante correo certificado con acuse de recibo a la entidad competente, la que consigna en su registro el número del certificado y la fecha de recepción.

131.2 El administrado exhibe al momento de su despacho el escrito en sobre abierto y cautela que el agente postal imprima su sello fechador tanto en su escrito como en el sobre.

131.3 En caso de duda, debe estarse a la fecha del sello estampado en el escrito, y, en su defecto, a la fecha de recepción por la entidad.

131.4 Esta modalidad no cabe para la presentación de recursos administrativos ni en procedimientos trilaterales.

(Texto según el artículo 120 de la Ley Nº 27444)

Artículo 132.- Recepción por medios alternativos
================================================

132.1 Los administrados que residan fuera de la provincia donde se ubica la unidad de recepción de la entidad competente pueden presentar los escritos dirigidos a otras dependencias de la entidad por intermedio del órgano desconcentrado ubicado en su lugar de domicilio.

132.2 Cuando las entidades no dispongan de servicios desconcentrados en el área de residencia del administrado, los escritos pueden ser presentados en las oficinas de las autoridades políticas del Ministerio del Interior del lugar de su domicilio.

132.3 Dentro de las veinticuatro horas inmediatas siguientes, dichas unidades remiten lo recibido a la autoridad destinataria mediante cualquier medio expeditivo a su alcance, indicando la fecha de su presentación.

(Texto según el artículo 121 de la Ley Nº 27444)

Artículo 133.- Presunción común a los medios de recepción alternativa
=====================================================================

Para los efectos de vencimiento de plazos, se presume que los escritos y comunicaciones presentados a través del correo certificado, de los órganos desconcentrados y de las autoridades del Ministerio del Interior, han ingresado en la entidad destinataria en la fecha y hora en que fueron entregados a cualquiera de las dependencias señaladas. Cuando se trate de solicitudes sujetas a silencio administrativo positivo, el plazo que dispone la entidad destinataria para resolver se computará desde la fecha de recepción por ésta.

En el caso que la entidad que reciba no sea la competente para resolver, remitirá los escritos y comunicaciones a la entidad de destino en el término de la distancia, la que informará al administrado de la fecha en que los recibe.

(Texto según el artículo 122 de la Ley Nº 27444)

Artículo 134.- Recepción por transmisión de datos a distancia
=============================================================

134.1 Los administrados pueden solicitar que el envío de información o documentación que le corresponda recibir dentro de un procedimiento sea realizado por medios de transmisión a distancia, tales como correo electrónico o facsímil.

134.2 Siempre que cuenten con sistemas de transmisión de datos a distancia, las entidades facilitan su empleo para la recepción de documentos o solicitudes y remisión de sus decisiones a los administrados.

134.3 Cuando se emplean medios de transmisión de datos a distancia, debe presentarse físicamente dentro del tercer día el escrito o la resolución respectiva, con cuyo cumplimiento se le entenderá recibido en la fecha de envío del correo electrónico o facsímil.(*)

(Texto según el artículo 123 de la Ley Nº 27444)

(*) De conformidad con la Segunda Disposición Complementaria Transitoria del Decreto Legislativo N° 1477, publicado el 08 mayo 2020, en caso las Municipalidades competentes de tramitar la Solicitud Única de Instalación de Infraestructura de Telecomunicaciones - SUIIT digital, cuenten con sistemas de transmisión de datos a distancia, se suspende la aplicación del numeral 3 del presente artículo, hasta el 31 de diciembre del 2020, o por el plazo que dure la Emergencia Sanitaria a nivel nacional declarada por Decreto Supremo Nº 008-2020-SA y/o ampliatorias, en caso ésta exceda la fecha señalada.

(*) De conformidad con la Cuarta Disposición Complementaria Transitoria del Decreto Legislativo N° 1497, publicado el 10 mayo 2020, se dispone la suspensión hasta el 31 de diciembre del año 2020 de la aplicación del numeral 123.3 del artículo 123 de la Ley Nº 27444, Ley del Procedimiento Administrativo General (Numeral 134.3 del Artículo 134 del TUO aprobado por el presente Decreto Supremo), en lo referido a la obligación de la presentación física del escrito o documentación por parte de los administrados. Cuando el administrado emplee medios de transmisión a distancia se considera como fecha de recepción la fecha en que se registre la documentación a través de los medios digitales empleados por la entidad. Dicha suspensión puede ser prorrogada mediante Decreto Supremo refrendado por el Presidente del Consejo de Ministros para fines de simplificación administrativa, gobierno digital o transformación digital del Estado.

Artículo 135.- Obligaciones de unidades de recepción
====================================================

135.1 Las unidades de recepción documental orientan al administrado en la presentación de sus solicitudes y formularios, quedando obligadas a recibirlos y darles ingreso para iniciar o impulsar los procedimientos, sin que en ningún caso pueda calificar, negar o diferir su admisión.

135.2 Quien recibe las solicitudes o formularios debe anotar bajo su firma en el propio escrito, la hora, fecha y lugar en que lo recibe, el número de fojas que contenga, la mención de los documentos acompañados y de la copia presentada. Como constancia de recepción, es entregada la copia presentada diligenciada con las anotaciones respectivas y registrada, sin perjuicio de otras modalidades adicionales, que por razón del trámite sea conveniente extender.

(Texto según el artículo 124 de la Ley Nº 27444)

Artículo 136.- Observaciones a documentación presentada
=======================================================

136.1 Deben ser recibidos todos los formularios o escritos presentados, no obstante incumplir los requisitos establecidos en la presente Ley, que no estén acompañados de los recaudos correspondientes o se encuentren afectados por otro defecto u omisión formal prevista en el TUPA, que amerite corrección. En un solo acto y por única vez, la unidad de recepción al momento de su presentación realiza las observaciones por incumplimiento de requisitos que no puedan ser salvadas de oficio, invitando al administrado a subsanarlas dentro de un plazo máximo de dos días hábiles.

136.2 La observación debe anotarse bajo firma del receptor en la solicitud y en la copia que conservará el administrado, con las alegaciones respectivas si las hubiere, indicando que, si así no lo hiciera, se tendrá por no presentada su petición.

136.3 Mientras esté pendiente la subsanación, son aplicables las siguientes reglas:

136.3.1 No procede el cómputo de plazos para que opere el silencio administrativo, ni para la presentación de la solicitud o el recurso.

136.3.2 No procede la aprobación automática del procedimiento administrativo, de ser el caso.

136.3.3 La unidad no cursa la solicitud o el formulario a la dependencia competente para sus actuaciones en el procedimiento.

136.4 Transcurrido el plazo sin que ocurra la subsanación, la entidad considera como no presentada la solicitud o formulario y la devuelve con sus recaudos cuando el interesado se apersone a reclamarles, reembolsándole el monto de los derechos de tramitación que hubiese abonado.

136.5 Si la documentación presentada no se ajusta a lo requerido impidiendo la continuación del procedimiento, lo cual por su naturaleza no pudo ser advertido por la unidad de recepción al momento de su presentación, así como si resultara necesaria una actuación del administrado para continuar con el procedimiento, la Administración, por única vez, deberá emplazar inmediatamente al administrado, a fin de que realice la subsanación correspondiente. Mientras esté pendiente dicha subsanación son aplicables las reglas establecidas en los numerales 136.3.1 y 136.3.2. De no subsanar oportunamente lo requerido, resulta de aplicación lo dispuesto en el numeral 136.4.

En este caso no resulta aplicable la queja a que se refiere el numeral 137.2 del artículo 137, salvo que la Administración emplace nuevamente al administrado a fin de que efectúe subsanaciones adicionales.

(Texto según el artículo 125 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

136.6 En caso de procedimientos administrativos que se inicien a través de medio electrónico, que no acompañen los recaudos correspondientes o adolezcan de otro defecto u omisión formal previstos en el TUPA que no puedan ser subsanados de oficio, la autoridad competente requiere la subsanación por el mismo medio, en un solo acto y por única vez en el plazo máximo de dos (2) días hábiles.

Corresponde al administrado presentar la información para subsanar el defecto u omisión en un plazo máximo de dos (2) días hábiles siguientes de efectuado el requerimiento de la autoridad competente. Mientras esté pendiente dicha subsanación son aplicables las reglas establecidas en los numerales 136.3.1 y 136.3.2. De no subsanarse oportunamente lo requerido resulta de aplicación lo dispuesto en el numeral 136.4.

(Numeral incorporado según el artículo 3 del Decreto Legislativo Nº 1452)

Artículo 137.- Subsanación documental
=====================================

137.1 Ingresado el escrito o formulada la subsanación debidamente, se considera recibido a partir del documento inicial, salvo que el procedimiento confiera prioridad registral o se trate de un procedimiento trilateral, en cuyo caso la presentación opera a partir de la subsanación.

137.2 Las entidades de la Administración Pública se encuentran obligadas a realizar una revisión integral del cumplimiento de todos los requisitos de las solicitudes que presentan los administrados y, en una sola oportunidad y en un solo documento, formular todas las observaciones y los requerimientos que correspondan.

Sin perjuicio de lo señalado en el párrafo precedente, la entidad mantiene la facultad de requerir única y exclusivamente la subsanación de aquellos requisitos que no hayan sido subsanados por el administrado o cuya subsanación no resulte satisfactoria, de conformidad con lo dispuesto por la norma correspondiente. En ningún caso la entidad podrá realizar nuevas observaciones invocando la facultad señalada en el presente párrafo.

137.3 El incumplimiento de esta obligación constituye una falta administrativa sancionable de conformidad con lo dispuesto por el artículo 261.

137.4 Sin perjuicio de lo anterior, el incumplimiento de esta obligación también constituye una barrera burocrática ilegal, siendo aplicables las sanciones establecidas en la normativa sobre prevención y eliminación de barreras burocráticas. Ello, sin perjuicio de la obligación del administrado de subsanar las observaciones formuladas.

(Texto según el artículo 126 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

Artículo 138.- Régimen de fedatarios
====================================

Cuando se establezcan requisitos de autenticación de documentos el administrado podrá acudir al régimen de fedatarios que se describe a continuación:

1. Cada entidad designa fedatarios institucionales adscritos a sus unidades de recepción documental, en número proporcional a sus necesidades de atención, quienes, sin exclusión de sus labores ordinarias, brindan gratuitamente sus servicios a los administrados.

2. El fedatario tiene como labor personalísima, comprobar y autenticar, previo cotejo entre el original que exhibe el administrado y la copia presentada, la fidelidad del contenido de esta última para su empleo en los procedimientos de la entidad, cuando en la actuación administrativa sea exigida la agregación de los documentos o el administrado desee agregados como prueba. También pueden, a pedido de los administrados, certificar firmas previa verificación de la identidad del suscriptor, para las actuaciones administrativas concretas en que sea necesario.

3. En caso de complejidad derivada del cúmulo o de la naturaleza de los documentos a autenticar, la oficina de trámite documentario consulta al administrado la posibilidad de retener los originales, para lo cual se expedirá una constancia de retención de los documentos al administrado, por el término máximo de dos días hábiles, para certificar las correspondientes reproducciones. Cumplido éste, devuelve al administrado los originales mencionados.

4. La entidad puede requerir en cualquier estado del procedimiento la exhibición del original presentado para la autenticación por el fedatario.

(Texto según el artículo 127 de la Ley Nº 27444)

Artículo 139.- Potestad administrativa para autenticar actos propios
====================================================================

La facultad para realizar autenticaciones atribuidas a los fedatarios no afecta la potestad administrativa de las autoridades para dar fe de la autenticidad de los documentos que ellos mismos hayan emitido.

(Texto según el artículo 128 de la Ley Nº 27444)

Artículo 140.- Ratificación de firma y del contenido de escrito
===============================================================

140.1 En caso de duda sobre la autenticidad de la firma del administrado o falta de claridad sobre los extremos de su petición, como primera actuación, la autoridad puede notificarlo para que dentro de un plazo prudencial ratifique la firma o aclare el contenido del escrito, sin perjuicio de la continuación del procedimiento.

140.2 La ratificación puede hacerla el administrado por escrito o apersonándose a la entidad, en cuyo caso se levantará el acta respectiva, que es agregada al expediente.

140.3 Procede la mejora de la solicitud por parte del administrado, en los casos a que se refiere este artículo.

(Texto según el artículo 129 de la Ley Nº 27444)

Artículo 141.- Presentación de escritos ante organismos incompetentes
=====================================================================

141.1 Cuando sea ingresada una solicitud que se estima competencia de otra entidad, la entidad receptora debe remitirla, en el término de la distancia, a aquélla que considere competente, comunicando dicha decisión al administrado. En este caso, el cómputo del plazo para resolver se iniciará en la fecha que la entidad competente recibe la solicitud.

141.2 Si la entidad aprecia su incompetencia pero no reúne certeza acerca de la entidad competente, notificará dicha situación al administrado para que adopte la decisión más conveniente a su derecho.

