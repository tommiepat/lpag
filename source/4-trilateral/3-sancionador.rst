======================================
CAPÍTULO III Procedimiento Sancionador
======================================

Artículo 247.- Ámbito de aplicación de este capítulo
====================================================

247.1 Las disposiciones del presente Capítulo disciplinan la facultad que se atribuye a cualquiera de las entidades para establecer infracciones administrativas y las consecuentes sanciones a los administrados.

247.2 Las disposiciones contenidas en el presente Capítulo se aplican con carácter supletorio a todos los procedimientos establecidos en leyes especiales, incluyendo los tributarios, los que deben observar necesariamente los principios de la potestad sancionadora administrativa a que se refiere el artículo 248, así como la estructura y garantías previstas para el procedimiento administrativo sancionador.

Los procedimientos especiales no pueden imponer condiciones menos favorables a los administrados, que las previstas en este Capítulo.

247.3 La potestad sancionadora disciplinaria sobre el personal de las entidades se rige por la normativa sobre la materia.

(Texto según el artículo 229 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

Artículo 248.- Principios de la potestad sancionadora administrativa
====================================================================

La potestad sancionadora de todas las entidades está regida adicionalmente por los siguientes principios especiales:

1. Legalidad.- Sólo por norma con rango de ley cabe atribuir a las entidades la potestad sancionadora y la consiguiente previsión de las consecuencias administrativas que a título de sanción son posibles de aplicar a un administrado, las que en ningún caso habilitarán a disponer la privación de libertad.

2. Debido procedimiento.- No se pueden imponer sanciones sin que se haya tramitado el procedimiento respectivo, respetando las garantías del debido procedimiento. Los procedimientos que regulen el ejercicio de la potestad sancionadora deben establecer la debida separación entre la fase instructora y la sancionadora, encomendándolas a autoridades distintas.

3. Razonabilidad.- Las autoridades deben prever que la comisión de la conducta sancionable no resulte más ventajosa para el infractor que cumplir las normas infringidas o asumir la sanción. Sin embargo, las sanciones a ser aplicadas deben ser proporcionales al incumplimiento calificado como infracción, observando los siguientes criterios que se señalan a efectos de su graduación:

a) El beneficio ilícito resultante por la comisión de la infracción;

b) La probabilidad de detección de la infracción;

c) La gravedad del daño al interés público y/o bien jurídico protegido;

d) EI perjuicio económico causado;

e) La reincidencia, por la comisión de la misma infracción dentro del plazo de un (1) año desde que quedó firme la resolución que sancionó la primera infracción.

f) Las circunstancias de la comisión de la infracción; y

g) La existencia o no de intencionalidad en la conducta del infractor.

4. Tipicidad.- Solo constituyen conductas sancionables administrativamente las infracciones previstas expresamente en normas con rango de ley mediante su tipificación como tales, sin admitir interpretación extensiva o analogía. Las disposiciones reglamentarias de desarrollo pueden especificar o graduar aquellas dirigidas a identificar las conductas o determinar sanciones, sin constituir nuevas conductas sancionables a las previstas legalmente, salvo los casos en que la ley o Decreto Legislativo permita tipificar infracciones por norma reglamentaria.

A través de la tipificación de infracciones no se puede imponer a los administrados el cumplimiento de obligaciones que no estén previstas previamente en una norma legal o reglamentaria, según corresponda.

En la configuración de los regímenes sancionadores se evita la tipificación de infracciones con idéntico supuesto de hecho e idéntico fundamento respecto de aquellos delitos o faltas ya establecidos en las leyes penales o respecto de aquellas infracciones ya tipificadas en otras normas administrativas sancionadoras.

5.- Irretroactividad.- Son aplicables las disposiciones sancionadoras vigentes en el momento de incurrir el administrado en la conducta a sancionar, salvo que las posteriores le sean más favorables.

Las disposiciones sancionadoras producen efecto retroactivo en cuanto favorecen al presunto infractor o al infractor, tanto en lo referido a la tipificación de la infracción como a la sanción y a sus plazos de prescripción, incluso respecto de las sanciones en ejecución al entrar en vigor la nueva disposición.

6. Concurso de Infracciones.- Cuando una misma conducta califique como más de una infracción se aplicará la sanción prevista para la infracción de mayor gravedad, sin perjuicio que puedan exigirse las demás responsabilidades que establezcan las leyes.

7. Continuación de infracciones.- Para determinar la procedencia de la imposición de sanciones por infracciones en las que el administrado incurra en forma continua, se requiere que hayan transcurrido por lo menos treinta (30) días hábiles desde la fecha de la imposición de la última sanción y que se acredite haber solicitado al administrado que demuestre haber cesado la infracción dentro de dicho plazo.

Las entidades, bajo sanción de nulidad, no podrán atribuir el supuesto de continuidad y/o la imposición de la sanción respectiva, en los siguientes casos:

a) Cuando se encuentre en trámite un recurso administrativo interpuesto dentro del plazo contra el acto administrativo mediante el cual se impuso la última sanción administrativa.

b) Cuando el recurso administrativo interpuesto no hubiera recaído en acto administrativo firme.

c) Cuando la conducta que determinó la imposición de la sanción administrativa original haya perdido el carácter de infracción administrativa por modificación en el ordenamiento, sin perjuicio de la aplicación de principio de irretroactividad a que se refiere el inciso 5.

8. Causalidad.- La responsabilidad debe recaer en quien realiza la conducta omisiva o activa constitutiva de infracción sancionable.

9. Presunción de licitud.- Las entidades deben presumir que los administrados han actuado apegados a sus deberes mientras no cuenten con evidencia en contrario.

10. Culpabilidad.- La responsabilidad administrativa es subjetiva, salvo los casos en que por ley o decreto legislativo se disponga la responsabilidad administrativa objetiva.

11. Non bis in idem.- No se podrán imponer sucesiva o simultáneamente una pena y una sanción administrativa por el mismo hecho en los casos en que se aprecie la identidad del sujeto, hecho y fundamento.

Dicha prohibición se extiende también a las sanciones administrativas, salvo la concurrencia del supuesto de continuación de infracciones a que se refiere el inciso 7.

(Texto según el artículo 230 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

Artículo 249.- Estabilidad de la competencia para la potestad sancionadora
==========================================================================

El ejercicio de la potestad sancionadora corresponde a las autoridades administrativas a quienes le hayan sido expresamente atribuidas por disposición legal o reglamentaria, sin que pueda asumirla o delegarse en órgano distinto.

(Texto según el artículo 231 de la Ley Nº 27444)

Artículo 250.- Reglas sobre el ejercicio de la potestad sancionadora.
=====================================================================

En virtud del principio de razonabilidad en el ámbito de los procedimientos administrativos sancionadores deberán observarse las siguientes reglas:

a) En el caso de infracciones administrativas pasibles de multas que tengan como fundamento el incumplimiento de la realización de trámites, obtención de licencias, permisos y autorizaciones u otros procedimientos similares ante autoridades competentes por concepto de instalación de infraestructuras en red para servicios públicos u obras públicas de infraestructura, exclusivamente en los casos en que ello sea exigido por el ordenamiento vigente, la cuantía de la sanción a ser impuesta no podrá exceder:

- El uno (1%) de valor de la obra o proyecto, según sea el caso.

- El cien por ciento (100%) del monto por concepto de la tasa aplicable por derecho de trámite, de acuerdo a Texto Único de Procedimientos Administrativos (TUPA) vigente en el momento de ocurrencia de los hechos, en los casos en que no sea aplicable la valoración indicada con anterioridad.

Los casos de imposición de multas administrativas por montos que excedan los límites señalados con anterioridad, serán conocidos por la Comisión de Acceso al Mercado del Instituto Nacional de Defensa de la Competencia y de la Protección de la Propiedad Intelectual,(2) para efectos de determinar si en tales supuestos se han constituido barreras burocráticas ilegales de acceso al mercado, conforme al procedimiento administrativo contemplado en el Decreto Ley Nº 25868 y el Decreto Legislativo Nº 807, y en sus normas modificatorias y complementarias.

b) Cuando el procedimiento sancionador recaiga sobre la carencia de autorización o licencia para la realización de varias conductas individuales que, atendiendo a la naturaleza de los hechos, importen la comisión de una actividad y/o proyecto que las comprendan en forma general, cuya existencia haya sido previamente comunicada a la entidad competente, la sanción no podrá ser impuesta en forma individualizada, sino aplicada en un concepto global atendiendo a los criterios previstos en el inciso 3 del artículo 248.

(Texto según el artículo 231-A de la Ley Nº 27444)

Artículo 251. -Determinación de la responsabilidad
==================================================

251.1 Las sanciones administrativas que se impongan al administrado son compatibles con el dictado de medidas correctivas conducentes a ordenar la reposición o la reparación de la situación alterada por la infracción a su estado anterior, incluyendo la de los bienes afectados, así como con la indemnización por los daños y perjuicios ocasionados, las que son determinadas en el proceso judicial correspondiente. Las medidas correctivas deben estar previamente tipificadas, ser razonables y ajustarse a la intensidad, proporcionalidad y necesidades de los bienes jurídicos tutelados que se pretenden garantizar en cada supuesto concreto.

251.2 Cuando el cumplimiento de las obligaciones previstas en una disposición legal corresponda a varias personas conjuntamente, responderán en forma solidaria de las infracciones que, en su caso, se cometan, y de las sanciones que se impongan.

(Texto según el artículo 232 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

Artículo 252.- Prescripción
===========================

252.1 La facultad de la autoridad para determinar la existencia de infracciones administrativas, prescribe en el plazo que establezcan las leyes especiales, sin perjuicio del cómputo de los plazos de prescripción respecto de las demás obligaciones que se deriven de los efectos de la comisión de la infracción. En caso ello no hubiera sido determinado, dicha facultad de la autoridad prescribirá a los cuatro (4) años.

252.2 EI cómputo del plazo de prescripción de la facultad para determinar la existencia de infracciones comenzará a partir del día en que la infracción se hubiera cometido en el caso de las infracciones instantáneas o infracciones instantáneas de efectos permanentes, desde el día que se realizó la última acción constitutiva de la infracción en el caso de infracciones continuadas, o desde el día en que la acción cesó en el caso de las infracciones permanentes.

EI cómputo del plazo de prescripción sólo se suspende con la iniciación del procedimiento sancionador a través de la notificación al administrado de los hechos constitutivos de infracción que les sean imputados a título de cargo, de acuerdo a lo establecido en el artículo 255, inciso 3. Dicho cómputo deberá reanudarse inmediatamente si el trámite del procedimiento sancionador se mantuviera paralizado por más de veinticinco (25) días hábiles, por causa no imputable al administrado.

252.3 La autoridad declara de oficio la prescripción y da por concluido el procedimiento cuando advierta que se ha cumplido el plazo para determinar la existencia de infracciones. Asimismo, los administrados pueden plantear la prescripción por vía de defensa y la autoridad debe resolverla sin más trámite que la constatación de los plazos.

En caso se declare la prescripción, la autoridad podrá iniciar las acciones necesarias para determinar las causas y responsabilidades de la inacción administrativa, solo cuando se advierta que se hayan producido situaciones de negligencia.

(Texto según el artículo 233 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

Artículo 253.- Prescripción de la exigibilidad de las multas impuestas
======================================================================

1. La facultad de la autoridad para exigir por la vía de ejecución forzosa el pago de las multas impuestas por la comisión de una infracción administrativa prescribe en el plazo que establezcan las leyes especiales. En caso de no estar determinado, la prescripción se produce al término de dos (2) años computados a partir de la fecha en que se produzca cualquiera de las siguientes circunstancias:

a) Que el acto administrativo mediante el cual se impuso la multa, o aquel que puso fin a la vía administrativa, quedó firme.

b) Que el proceso contencioso administrativo destinado a la impugnación del acto mediante el cual se impuso la multa haya concluido con carácter de cosa juzgada en forma desfavorable para el administrado.

2. El cómputo del plazo de prescripción se suspende en los siguientes supuestos:

a) Con la iniciación del procedimiento de ejecución forzosa, conforme a los mecanismos contemplados en el artículo 207, según corresponda. Dicho cómputo debe reanudarse inmediatamente en caso que se configure alguno de los supuestos de suspensión del procedimiento de ejecución forzosa que contemple el ordenamiento vigente y/o se produzca cualquier causal que determine la paralización del procedimiento por más de veinticinco (25) días hábiles.

b) Con la presentación de la demanda de revisión judicial del procedimiento de ejecución forzosa o cualquier otra disposición judicial que suspenda la ejecución forzosa, conforme al ordenamiento vigente. La suspensión del cómputo opera hasta la notificación de la resolución que declara concluido el proceso con calidad de cosa juzgada en forma desfavorable al administrado.

(Texto modificado según el artículo 2 del Decreto Legislativo Nº 1452)

3. Los administrados pueden deducir la prescripción como parte de la aplicación de los mecanismos de defensa previstos dentro del procedimiento de ejecución forzosa. La autoridad competente debe resolverla sin más trámite que la constatación de los plazos, pudiendo en los casos de estimarla fundada, disponer el inicio de las acciones de responsabilidad para dilucidar las causales de la inacción administrativa, solo cuando se advierta se hayan producido situaciones de negligencia.

En caso que la prescripción sea deducida en sede administrativa, el plazo máximo para resolver sobre la solicitud de suspensión de la ejecución forzosa por prescripción es de ocho (8) días hábiles contados a partir de la presentación de dicha solicitud por el administrado. Vencido dicho plazo sin que exista pronunciamiento expreso, se entiende concedida la solicitud, por aplicación del silencio administrativo positivo.

(Artículo incorporado por el artículo 4 del Decreto Legislativo Nº 1272)

Artículo 254.- Caracteres del procedimiento sancionador
=======================================================

254.1 Para el ejercicio de la potestad sancionadora se requiere obligatoriamente haber seguido el procedimiento legal o reglamentariamente establecido caracterizado por:

1. Diferenciar en su estructura entre la autoridad que conduce la fase instructora y la que decide la aplicación de la sanción.

2. Considerar que los hechos probados por resoluciones judiciales firmes vinculan a las entidades en sus procedimientos sancionadores.

3. Notificar a los administrados los hechos que se le imputen a título de cargo, la calificación de las infracciones que tales hechos pueden constituir y la expresión de las sanciones que, en su caso, se le pudiera imponer, así como la autoridad competente para imponer la sanción y la norma que atribuya tal competencia.

4. Otorgar al administrado un plazo de cinco días para formular sus alegaciones y utilizar los medios de defensa admitidos por el ordenamiento jurídico conforme al numeral 173.2 del artículo 173, sin que la abstención del ejercicio de este derecho pueda considerarse elemento de juicio en contrario a su situación.

254.2 La Administración revisa de oficio las resoluciones administrativas fundadas en hechos contradictorios con los probados en las resoluciones judiciales con calidad de cosa juzgada, de acuerdo con las normas que regulan los procedimientos de revisión de oficio.

(Texto según el artículo 234 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

Artículo 255.- Procedimiento sancionador
========================================

Las entidades en el ejercicio de su potestad sancionadora se ciñen a las siguientes disposiciones:

1. El procedimiento sancionador se inicia siempre de oficio, bien por propia iniciativa o como consecuencia de orden superior, petición motivada de otros órganos o entidades o por denuncia.

2. Con anterioridad a la iniciación formal del procedimiento se podrán realizar actuaciones previas de investigación, averiguación e inspección con el objeto de determinar con carácter preliminar si concurren circunstancias que justifiquen su iniciación.

3. Decidida la iniciación del procedimiento sancionador, la autoridad instructora del procedimiento formula la respectiva notificación de cargo al posible sancionado, la que debe contener los datos a que se refiere el numeral 3 del artículo precedente para que presente sus descargos por escrito en un plazo que no podrá ser inferior a cinco días hábiles contados a partir de la fecha de notificación.

4. Vencido dicho plazo y con el respectivo descargo o sin él, la autoridad que instruye el procedimiento realizará de oficio todas las actuaciones necesarias para el examen de los hechos, recabando los datos e informaciones que sean relevantes para determinar, en su caso, la existencia de responsabilidad susceptible de sanción.

5. Concluida, de ser el caso, la recolección de pruebas, la autoridad instructora del procedimiento concluye determinando la existencia de una infracción y, por ende, la imposición de una sanción; o la no existencia de infracción. La autoridad instructora formula un informe final de instrucción en el que se determina, de manera motivada, las conductas que se consideren probadas constitutivas de infracción, la norma que prevé la imposición de sanción; y, la sanción propuesta o la declaración de no existencia de infracción, según corresponda.

Recibido el informe final, el órgano competente para decidir la aplicación de la sanción puede disponer la realización de actuaciones complementarias, siempre que las considere indispensables para resolver el procedimiento. El informe final de instrucción debe ser notificado al administrado para que formule sus descargos en un plazo no menor de cinco (5) días hábiles.

6. La resolución que aplique la sanción o la decisión de archivar el procedimiento será notificada tanto al administrado como al órgano u entidad que formuló la solicitud o a quién denunció la infracción, de ser el caso.

(Texto según el artículo 235 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

Artículo 256.- Medidas de carácter provisional
==============================================

256.1 La autoridad que tramita el procedimiento puede disponer, en cualquier momento, la adopción de medidas de carácter provisional que aseguren la eficacia de la resolución final que pudiera recaer, con sujeción a lo previsto por el artículo 157.

256.2 Las medidas que se adopten deberán ajustarse a la intensidad, proporcionalidad y necesidad de los objetivos que se pretende garantizar en cada supuesto concreto.

256.3 No se puede dictar medidas de carácter provisional que puedan causar perjuicio de difícil o imposible reparación a los interesados o que impliquen violación de sus derechos.

256.4 Las medidas de carácter provisional no pueden extenderse más allá de lo que resulte indispensable para cumplir los objetivos cautelares concurrentes en el caso concreto.

256.5 Durante la tramitación, la autoridad competente que hubiese ordenado las medidas de carácter provisional las revoca, de oficio o a instancia de parte, cuando compruebe que ya no son indispensables para cumplir los objetivos cautelares concurrentes en el caso concreto.

256.6 Cuando la autoridad constate, de oficio o a instancia de parte, que se ha producido un cambio de la situación que tuvo en cuenta al tomar la decisión provisional, esta debe ser cambiada, modificando las medidas provisionales acordadas o sustituyéndolas por otras, según requiera la nueva medida.

256.7 El cumplimiento o ejecución de las medidas de carácter provisional que en su caso se adopten, se compensan, en cuanto sea posible, con la sanción impuesta.

256.8 Las medidas de carácter provisional se extinguen por las siguientes causas:

1. Por la resolución que pone fin al procedimiento en que se hubiesen ordenado. La autoridad competente para resolver el recurso administrativo de que se trate puede, motivadamente, mantener las medidas acordadas o adoptar otras hasta que dicte el acto de resolución del recurso.

2. Por la caducidad del procedimiento sancionador.

(Texto según el artículo 236 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

Artículo 257.- Eximentes y atenuantes de responsabilidad por infracciones
=========================================================================

1.- Constituyen condiciones eximentes de la responsabilidad por infracciones las siguientes:

a) El caso fortuito o la fuerza mayor debidamente comprobada.

b) Obrar en cumplimiento de un deber legal o el ejercicio legítimo del derecho de defensa.

c) La incapacidad mental debidamente comprobada por la autoridad competente, siempre que esta afecte la aptitud para entender la infracción.

d) La orden obligatoria de autoridad competente, expedida en ejercicio de sus funciones.

e) El error inducido por la Administración o por disposición administrativa confusa o ilegal.

f) La subsanación voluntaria por parte del posible sancionado del acto u omisión imputado como constitutivo de infracción administrativa, con anterioridad a la notificación de la imputación de cargos a que se refiere el inciso 3) del artículo 255.

2.- Constituyen condiciones atenuantes de la responsabilidad por infracciones las siguientes:

a) Si iniciado un procedimiento administrativo sancionador el infractor reconoce su responsabilidad de forma expresa y por escrito.

En los casos en que la sanción aplicable sea una multa esta se reduce hasta un monto no menor de la mitad de su importe.

b) Otros que se establezcan por norma especial. (*)

(Texto según el artículo 236-A de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

(*) De conformidad con el Artículo 1 de la Resolución N° 064-2019-OSINFOR-TFFS-I, publicada el 06 diciembre 2019, se establece como criterio interpretativo de carácter general que procederá la aplicación de la condición atenuante de responsabilidad administrativa estipulada en el literal a) del numeral 2 del presente artículo, reduciéndose el cincuenta por ciento (50%) de la multa a imponer, en aquellos casos que el administrado mediante una manifestación inequívoca, indubitable y expresa que deberá cumplir con los presupuestos de voluntad, oportunidad e incondicionalidad, establecidos en la citada resolución, reconozca la misma en el escrito de descargos al acto administrativo que da inicio al procedimiento administrativo único y dentro del plazo otorgado para presentarlos, siempre y cuando tampoco cuestione la determinación de la responsabilidad administrativa. Asimismo, no cabe la aplicación del atenuante mencionado, para el reconocimiento realizado en la interposición de los recursos administrativos, salvo que se argumente la inaplicación del mismo.

Artículo 258.- Resolución
=========================

258.1 En la resolución que ponga fin al procedimiento no se podrán aceptar hechos distintos de los determinados en el curso del procedimiento, con independencia de su diferente valoración jurídica.

258.2 La resolución será ejecutiva cuando ponga fin a la vía administrativa. La administración podrá adoptar las medidas cautelares precisas para garantizar su eficacia, en tanto no sea ejecutiva.

258.3 Cuando el infractor sancionado recurra o impugne la resolución adoptada, la resolución de los recursos que interponga no podrá determinar la imposición de sanciones más graves para el sancionado.

(Texto según el artículo 237 de la Ley Nº 27444)

Artículo 259.- Caducidad administrativa del procedimiento sancionador
=====================================================================

1. El plazo para resolver los procedimientos sancionadores iniciados de oficio es de nueve (9) meses contado desde la fecha de notificación de la imputación de cargos. Este plazo puede ser ampliado de manera excepcional, como máximo por tres (3) meses, debiendo el órgano competente emitir una resolución debidamente sustentada, justificando la ampliación del plazo, previo a su vencimiento. La caducidad administrativa no aplica al procedimiento recursivo.

Cuando conforme a ley las entidades cuenten con un plazo mayor para resolver la caducidad operará al vencimiento de este.

2. Transcurrido el plazo máximo para resolver, sin que se notifique la resolución respectiva, se entiende automáticamente caducado administrativamente el procedimiento y se procederá a su archivo.

3. La caducidad administrativa es declarada de oficio por el órgano competente. El administrado se encuentra facultado para solicitar la caducidad administrativa del procedimiento en caso el órgano competente no la haya declarado de oficio.

4. En el supuesto que la infracción no hubiera prescrito, el órgano competente evaluará el inicio de un nuevo procedimiento sancionador. El procedimiento caducado administrativamente no interrumpe la prescripción.

5. La declaración de la caducidad administrativa no deja sin efecto las actuaciones de fiscalización, así como los medios probatorios que no puedan o no resulte necesario ser actuados nuevamente. Asimismo, las medidas preventivas, correctivas y cautelares dictadas se mantienen vigentes durante el plazo de tres (3) meses adicionales en tanto se disponga el inicio del nuevo procedimiento sancionador, luego de lo cual caducan, pudiéndose disponer nuevas medidas de la misma naturaleza en caso se inicie el procedimiento sancionador.

(Artículo incorporado por el artículo 4 del Decreto Legislativo Nº 1272, modificado según el artículo 2 del Decreto Legislativo Nº 1452)

