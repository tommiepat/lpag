========================================================
CAPÍTULO II LA ACTIVIDAD ADMINISTRATIVA DE FISCALIZACIÓN
========================================================

(Capítulo I-A incorporado por el artículo 5 del Decreto Legislativo Nº 1272)

Artículo 239.- Definición de la actividad de fiscalización
==========================================================

239.1 La actividad de fiscalización constituye el conjunto de actos y diligencias de investigación, supervisión, control o inspección sobre el cumplimiento de las obligaciones, prohibiciones y otras limitaciones exigibles a los administrados, derivados de una norma legal o reglamentaria, contratos con el Estado u otra fuente jurídica, bajo un enfoque de cumplimiento normativo, de prevención del riesgo, de gestión del riesgo y tutela de los bienes jurídicos protegidos.

Solamente por Ley o Decreto Legislativo puede atribuirse la actividad de fiscalización a las entidades.

Por razones de eficacia y economía, las autoridades pueden coordinar para la realización de acciones de fiscalización conjunta o realizar encargos de gestión entre sí.

239.2 Independientemente de su denominación, las normas especiales que regulan esta función se interpretan y aplican en el marco de las normas comunes del presente capítulo, aun cuando conforme al marco legal sean ejercidos por personas naturales o jurídicas privadas.

Artículo 240.- Facultades de las entidades que realizan actividad de fiscalización
==================================================================================

240.1 Los actos y diligencias de fiscalización se inician siempre de oficio, bien por propia iniciativa o como consecuencia de orden superior, petición motivada o por denuncia.

240.2 La Administración Pública en el ejercicio de la actividad de fiscalización está facultada para realizar lo siguiente:

1. Requerir al administrado objeto de la fiscalización, la exhibición o presentación de todo tipo de documentación, expedientes, archivos u otra información necesaria, respetando el principio de legalidad.

El acceso a la información que pueda afectar la intimidad personal o familiar, así como las materias protegidas por el secreto bancario, tributario, comercial e industrial y la protección de datos personales, se rige por lo dispuesto en la Constitución Política del Perú y las leyes especiales.

2. Interrogar a las personas materia de fiscalización o a sus representantes, empleados, funcionarios, asesores y a terceros, utilizando los medios técnicos que considere necesarios para generar un registro completo y fidedigno de sus declaraciones.

La citación o la comparecencia personal a la sede de las entidades administrativas se regulan por los artículos 69 y 70.

3. Realizar inspecciones, con o sin previa notificación, en los locales y/o bienes de las personas naturales o jurídicas objeto de las acciones de fiscalización, respetando el derecho fundamental a la inviolabilidad del domicilio cuando corresponda.

4. Tomar copia de los archivos físicos, ópticos, electrónicos u otros, así como tomar fotografías, realizar impresiones, grabaciones de audio o en video con conocimiento previo del administrado y, en general, utilizar los medios necesarios para generar un registro completo y fidedigno de su acción de fiscalización.

5. Realizar exámenes periciales sobre la documentación y otros aspectos técnicos relacionados con la fiscalización.

6. Utilizar en las acciones y diligencias de fiscalización equipos que consideren necesarios. Los administrados deben permitir el acceso de tales equipos, así como permitir el uso de sus propios equipos, cuando sea indispensable para la labor de fiscalización.

7. Ampliar o variar el objeto de la acción de fiscalización en caso que, como resultado de las acciones y diligencias realizadas, se detecten incumplimientos adicionales a los expresados inicialmente en el referido objeto.

8. Las demás que establezcan las leyes especiales.

Artículo 241.- Deberes de las entidades que realizan actividad de fiscalización
===============================================================================

241.1 La Administración Pública ejerce su actividad de fiscalización con diligencia, responsabilidad y respeto a los derechos de los administrados, adoptando las medidas necesarias para obtener los medios probatorios idóneos que sustenten los hechos verificados, en caso corresponda.

241.2 Las autoridades competentes tienen, entre otras, los siguientes deberes en el ejercicio de la actividad de fiscalización:

1. Previamente a las acciones y diligencias de fiscalización, realizar la revisión y/o evaluación de la documentación que contenga información relacionada con el caso concreto objeto de fiscalización.

2. Identificarse a requerimiento de los administrados, presentando la credencial otorgada por su entidad, así como su documento nacional de identidad.

3. Citar la base legal que sustente su competencia de fiscalización, sus facultades y obligaciones, al administrado que lo solicite.

4. Entregar copia del Acta de Fiscalización o documento que haga sus veces al administrado al finalizar la diligencia de inspección, consignando de manera clara y precisa las observaciones que formule el administrado.

5. Guardar reserva sobre la información obtenida en la fiscalización.

6. Deber de imparcialidad y prohibición de mantener intereses en conflicto.

Artículo 242.- Derechos de los administrados fiscalizados
=========================================================

Son derechos de los administrados fiscalizados:

1. Ser informados del objeto y del sustento legal de la acción de supervisión y, de ser previsible, del plazo estimado de su duración, así como de sus derechos y obligaciones en el curso de tal actuación.

2. Requerir las credenciales y el documento nacional de identidad de los funcionarios, servidores o terceros a cargo de la fiscalización.

3. Poder realizar grabaciones en audio o video de las diligencias en las que participen.

4. Se incluyan sus observaciones en las actas correspondientes.

5. Presentar documentos, pruebas o argumentos adicionales con posterioridad a la recepción del acta de fiscalización.

6. Llevar asesoría profesional a las diligencias si el administrado lo considera.

Artículo 243.- Deberes de los administrados fiscalizados
========================================================

Son deberes de los administrados fiscalizados:

1. Realizar o brindar todas las facilidades para ejecutar las facultades listadas en el artículo 240.

2. Permitir el acceso de los funcionarios, servidores y terceros fiscalizadores, a sus dependencias, instalaciones, bienes y/o equipos, de administración directa o no, sin perjuicio de su derecho fundamental a la inviolabilidad del domicilio cuando corresponda.

3. Suscribir el acta de fiscalización.

4. Las demás que establezcan las leyes especiales.

Artículo 244.- Contenido mínimo del Acta de Fiscalización
=========================================================

244.1 El Acta de Fiscalización o documento que haga sus veces, es el documento que registra las verificaciones de los hechos constatados objetivamente y contiene como mínimo los siguientes datos:

1. Nombre de la persona natural o razón social de la persona jurídica fiscalizada.

2. Lugar, fecha y hora de apertura y de cierre de la diligencia.

3. Nombre e identificación de los fiscalizadores.

4. Nombres e identificación del representante legal de la persona jurídica fiscalizada o de su representante designado para dicho fin.

5. Los hechos materia de verificación y/u ocurrencias de la fiscalización.

6. Las manifestaciones u observaciones de los representantes de los fiscalizados y de los fiscalizadores.

7. La firma y documento de identidad de las personas participantes. Si alguna de ellas se negara a firmar, se deja constancia de la negativa en el acta, sin que esto afecte su validez.

8. La negativa del administrado de identificarse y suscribir el acta.

244.2 Las Actas de fiscalización dejan constancia de los hechos verificados durante la diligencia, salvo prueba en contrario.

Artículo 245.- Conclusión de la actividad de fiscalización
==========================================================

245.1 Las actuaciones de fiscalización podrán concluir en:

1. La certificación o constancia de conformidad de la actividad desarrollada por el administrado.

2. La recomendación de mejoras o correcciones de la actividad desarrollada por el administrado.

3. La advertencia de la existencia de incumplimientos no susceptibles de ameritar la determinación de responsabilidades administrativas.

4. La recomendación del inicio de un procedimiento con el fin de determinar las responsabilidades administrativas que correspondan.

5. La adopción de medidas correctivas.

6. Otras formas según lo establezcan las leyes especiales.

245.2. Las entidades procurarán realizar algunas fiscalizaciones únicamente con finalidad orientativa, esto es, de identificación de riesgos y notificación de alertas a los administrados con la finalidad de que mejoren su gestión.

Artículo 246.- Medidas cautelares y correctivas
===============================================

Las entidades solo podrán dictar medidas cautelares y correctivas siempre que estén habilitadas por Ley o Decreto Legislativo y mediante decisión debidamente motivada y observando el Principio de Proporcionalidad.

