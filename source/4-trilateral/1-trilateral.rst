TÍTULO IV Del procedimiento trilateral, del procedimiento sancionador y la actividad administrativa de fiscalización

(Denominación modificada por el artículo 3 del Decreto Legislativo Nº 1272)

===================================
CAPÍTULO I Procedimiento trilateral
===================================

Artículo 229.- Procedimiento trilateral
=======================================

229.1 El procedimiento trilateral es el procedimiento administrativo contencioso seguido entre dos o más administrados ante las entidades de la administración y para los descritos en el inciso 8) del artículo I del Título Preliminar de la presente Ley.

229.2 La parte que inicia el procedimiento con la presentación de una reclamación será designada como “reclamante” y cualquiera de los emplazados será designado como “reclamado”.

(Texto según el artículo 219 de la Ley Nº 27444)

Artículo 230.- Marco legal
==========================

El procedimiento trilateral se rige por lo dispuesto en el presente Capítulo y en lo demás por lo previsto en esta Ley. Respecto de los procedimientos administrativos trilaterales regidos por leyes especiales, este capítulo tendrá únicamente carácter supletorio.

(Texto según el artículo 220 de la Ley Nº 27444)

Artículo 231.- Inicio del procedimiento
=======================================

231.1 El procedimiento trilateral se inicia mediante la presentación de una reclamación o de oficio.

231.2 Durante el desarrollo del procedimiento trilateral la administración debe favorecer y facilitar la solución conciliada de la controversia.

231.3 Una vez admitida a trámite la reclamación se pondrá en conocimiento del reclamado a fin de que éste presente su descargo.

(Texto según el artículo 221 de la Ley Nº 27444)

Artículo 232.- Contenido de la reclamación
==========================================

232.1 La reclamación deberá contener los requisitos de los escritos previstos en el artículo 124, así como el nombre y la dirección de cada reclamado, los motivos de la reclamación y la petición de sanciones u otro tipo de acción afirmativa.

232.2 La reclamación deberá ofrecer las pruebas y acompañará como anexos las pruebas de las que disponga.

232.3 La autoridad podrá solicitar aclaración de la reclamación de admitirla, cuando existan dudas en la exposición de los hechos o fundamentos de derecho respectivos.

(Texto según el artículo 222 de la Ley Nº 27444)

Artículo 233.- Contestación de la reclamación
=============================================

233.1 El reclamado deberá presentar la contestación de la reclamación dentro de los quince (15) días posteriores a la notificación de ésta; vencido este plazo, la Administración declarará en rebeldía al reclamado que no la hubiera presentado. La contestación deberá contener los requisitos de los escritos previstos en el artículo 124, así como la absolución de todos los asuntos controvertidos de hecho y de derecho, Las alegaciones y los hechos relevantes de la reclamación, salvo que hayan sido específicamente negadas en la contestación, se tendrán por aceptadas o merituadas como ciertas.

233.2 Las cuestiones se proponen conjunta y únicamente al contestar la reclamación o la réplica y son resueltas con la resolución final.

233.3 En el caso de que el reclamado no cumpla con presentar la contestación dentro del plazo establecido, la administración podrá permitir, si lo considera apropiado y razonable, la entrega de la contestación luego del vencimiento del plazo.

233.4 Adicionalmente a la contestación, el reclamado podrá presentar una réplica alegando violaciones a la legislación respectiva, dentro de la competencia del organismo correspondiente de la entidad. La presentación de réplicas y respuestas a aquellas réplicas se rige por las reglas para la presentación y contestación de reclamaciones, excluyendo lo referente a los derechos administrativos de trámite.

(Texto según el artículo 223 de la Ley Nº 27444)

Artículo 234.- Prohibición de responder a las contestaciones
============================================================

La réplica a las contestaciones de las reclamaciones, no está permitida. Los nuevos problemas incluidos en la contestación del denunciado serán considerados como materia controvertida.

(Texto según el artículo 224 de la Ley Nº 27444)

Artículo 235.- Pruebas
======================

Sin perjuicio de lo establecido en los artículos 173 a 191, la administración sólo puede prescindir de la actuación de las pruebas ofrecidas por cualquiera de las partes por acuerdo unánime de éstas.

(Texto según el artículo 225 de la Ley Nº 27444)

Artículo 236.- Medidas cautelares
=================================

236.1 En cualquier etapa del procedimiento trilateral, de oficio o a pedido de parte, podrán dictarse medidas cautelares conforme al artículo 146.

236.2 Si el obligado a cumplir con una medida cautelar ordenado por la administración no lo hiciere, se aplicarán las normas sobre ejecución forzosa prevista en los artículos 203 al 211.

236.3 Cabe la apelación contra la resolución que dicta una medida cautelar solicitada por alguna de las partes dentro del plazo de tres (3) días contados a partir de la notificación de la resolución que dicta la medida. Salvo disposición legal o decisión de la autoridad en contrario, la apelación no suspende la ejecución de la medida cautelar.

La apelación deberá elevarse al superior jerárquico en un plazo máximo de (1) día, contado desde la fecha de la concesión del recurso respectivo y será resuelta en un plazo de cinco (5) días.

(Texto según el artículo 226 de la Ley Nº 27444)

Artículo 237.- Impugnación
==========================

237.1 Contra la resolución final recaída en un procedimiento trilateral expedida por una autoridad u órgano sometido a subordinación jerárquica, sólo procede la interposición del recurso de apelación. De no existir superior jerárquico, sólo cabe plantear recurso de reconsideración.

237.2 La apelación deberá ser interpuesta ante el órgano que dictó la resolución apelada dentro de los quince (15) días de producida la notificación respectiva. El expediente respectivo deberá elevarse al superior jerárquico en un plazo máximo de dos (2) días contados desde la fecha de la concesión del recurso respectivo.

237.3 Dentro de los quince (15) días de recibido el expediente por el superior jerárquico se correrá traslado a la otra parte y se le concederá plazo de quince (15) días para la absolución de la apelación.

237.4 Con la absolución de la otra parte o vencido el plazo a que se refiere el artículo precedente, la autoridad que conoce de la apelación podrá señalar día y hora para la vista de la causa que no podrá realizarse en un plazo mayor de diez (10) días contados desde la fecha en que se notifique la absolución de la apelación a quien la interponga.

237.5 La administración deberá emitir resolución dentro de los treinta (30) días siguientes a la fecha de realización de la audiencia.

(Texto según el artículo 227 de la Ley Nº 27444)

Artículo 238.- Conciliación, transacción extrajudicial y desistimiento
======================================================================

238.1 En los casos en los que la Ley lo permita y antes de que se notifique la resolución final, la autoridad podrá aprobar acuerdos, pactos, convenios o contratos con los administrados que importen una transacción extrajudicial o conciliación, con el alcance, requisitos, efectos y régimen jurídico específico que en cada caso prevea la disposición que lo regule, pudiendo tales actos poner fin al procedimiento administrativo y dejar sin efecto las resoluciones que se hubieren dictado en el procedimiento. El acuerdo podrá ser recogido en una resolución administrativa.

238.2 Los citados instrumentos deberán constar por escrito y establecer como contenido mínimo la identificación de las partes intervinientes y el plazo de vigencia.

238.3 Al aprobar los acuerdos a que se refiere el numeral 238.1, la autoridad podrá continuar el procedimiento de oficio si del análisis de los hechos considera que podría estarse afectando intereses de terceros o la acción suscitada por la iniciación del procedimiento entrañase interés general.

238.4 Procede el desistimiento conforme a lo regulado en los artículos 200 y 201.

(Texto según el artículo 228 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

