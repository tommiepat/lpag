.. TUO LPAG documentation master file, created by
   sphinx-quickstart on Fri Feb  5 11:38:51 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

TUO Ley del Procedimiento Administrativo General
================================================

.. toctree::
   :caption: Título Preliminar
   :name: tit-pre
   :maxdepth: 2

   Capitulo Único                       <0-preliminar/1-unico>

.. toctree::
   :caption: T1 Regimen juridico de los actos adm
   :name: titulo-i
   :maxdepth: 2

   C1 De los actos administrativos      <1-juridico/1-general>
   C2 Nulidad del acto administrativo   <1-juridico/2-nulidad>
   C3 Eficacia del acto administrativo  <1-juridico/3-eficacia>

.. toctree::
   :caption: T2 Del Procedimiento Administrativo
   :name: titulo-ii
   :maxdepth: 2

   C1 Disposiciones Generales           <2-administrativo/1-general>
   C2 De los Sujetos del Procedimiento  <2-administrativo/2-sujetos>
   C3 Iniciacion del Procedimiento      <2-administrativo/3-iniciacion>
   C4 Plazos y Terminos                 <2-administrativo/4-plazos>
   C5 Ordenacion del Procedimiento      <2-administrativo/5-ordenacion>
   C6 Instruccion del Procedimiento     <2-administrativo/6-instruccion>
   C7 Participacion de Administrados    <2-administrativo/7-participacion>
   C8 Fin del Procedimiento             <2-administrativo/8-fin>
   C9 Ejecucion de Resoluciones         <2-administrativo/9-ejecucion>

.. toctree::
   :caption: T3 Revision de los Actos en Via Administrativa
   :name: titulo-iii
   :maxdepth: 2

   C1 Revision de Oficio                <3-revision/1-revision>
   C2 Recursos Administrativos          <3-revision/2-recursos>

.. toctree::
   :caption: T4 Proc trilateral, sancionador; fiscalizacion
   :name: titulo-iv
   :maxdepth: 2

   C1 Procedimiento Trilateral          <4-trilateral/1-trilateral>
   C2 Fiscalizacion                     <4-trilateral/2-fiscalizacion>
   C3 Proceso Sancionador               <4-trilateral/3-sancionador>

.. toctree::
   :caption: T5 Responsabilidad de la Administracion Publica
   :name: titulo-v
   :maxdepth: 2

   C1 De la Administracion Publica      <5-responsabilidad/1-administracion>
   C2 De las Autoridades y Personal     <5-responsabilidad/2-autoridades>

.. toctree::
   :caption: Disposiciones Complementarias
   :name: disp-comp
   :maxdepth: 2

   Disp Comp Finales                    <6-disposiciones/1-finales>
   Disp Comp Transitorias               <6-disposiciones/2-transitorias>
   Disp Comp Derogatorias               <6-disposiciones/3-derogatorias>

Indices y tablas
================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

------

Esta versión fue compilada el |today|.
