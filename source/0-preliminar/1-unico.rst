Decreto Supremo que aprueba el Texto Único Ordenado de la Ley Nº 27444 - Ley del Procedimiento Administrativo General
DECRETO SUPREMO Nº 004-2019-JUS

(SEPARATA ESPECIAL)

(*) NOTA: De conformidad con lo señalado en la Exposición de Motivos del presente decreto supremo, los textos únicos ordenados no modifican el valor y fuerza de las normas ordenadas, por ende, no crean nuevas normas. Por lo tanto, el TUO de la Ley Nº 27444, tiene como única finalidad reunir y sistematizar en un solo texto integral las normas del referido dispositivo legal a efectos de darle la coherencia sistemática que pudiera haber sido afectada como producto de las modificaciones, incorporaciones y derogaciones normativas.

(*) De conformidad con la Única Disposición Complementaria Transitoria del Reglamento aprobado por el Artículo 1 del Decreto Supremo N° 022-2019-VIVIENDA, publicado el 30 julio 2019, se dispone que hasta la entrada en vigencia del Reglamento de Supervisión que establece la Tercera Disposición Complementaria Final, las acciones de supervisión se rigen por las disposiciones contenidas en el Texto Único Ordenado de la Ley Nº 27444, Ley del Procedimiento Administrativo General, aprobado mediante el presente Decreto Supremo, en la Directiva de Órgano Nº 001-2015-VIVIENDA-VMCS/DGAA “Lineamientos para la Supervisión de las Obligaciones Ambientales Fiscalizables de la Dirección General de Asuntos Ambientales” aprobada mediante Resolución Directoral Nº 149-2015-VIVIENDA-VMCS-DGAA y por las demás normas que resulten aplicables. El Reglamento de Medidas Administrativas en materia ambiental del Ministerio de Vivienda, Construcción y Saneamiento, es aplicable a partir de la entrada en vigencia del Reglamento del Procedimiento Administrativo Sancionador en materia ambiental del Ministerio de Vivienda, Construcción y Saneamiento, aprobado por Decreto Supremo Nº 018-2019-VIVIENDA.

CONCORDANCIAS:     D.S.N° 164-2020-PCM (Decreto Supremo que aprueba el Procedimiento Administrativo Estandarizado de Acceso a la Información Pública creada u obtenida por la entidad, que se encuentre en su posesión o bajo su control)

Acuerdo N° 009-2020-TCE (Acuerdo de Sala Plena que establece disposiciones para la notificación del inicio de procedimiento administrativo sancionador)

D.S.N° 200-2020-PCM (Decreto Supremo que aprueba Procedimientos Administrativos Estandarizados de Licencia de Funcionamiento y de Licencia Provisional de Funcionamiento para Bodegas)

Enlace Web: EXPOSICIÓN DE MOTIVOS - PDF.

NOTA: Esta Exposición de Motivos no ha sido publicada en el diario oficial “El Peruano”, ha sido enviada por la Secretaría General del Ministerio de Justicia y Derechos Humanos, mediante Memorando Nº 139-2019-JUS/SG, de fecha 11 de febrero de 2019.

EL PRESIDENTE DE LA REPÚBLICA

CONSIDERANDO:

Que, mediante la Ley Nº 27444, Ley del Procedimiento Administrativo General, se establecen las normas comunes para las actuaciones de la función administrativa del Estado y, regula todos los procedimientos administrativos desarrollados en las entidades, incluyendo los procedimientos especiales;

Que, mediante Decreto Legislativo Nº 1272, Decreto Legislativo que modifica la Ley Nº 27444, Ley del Procedimiento Administrativo General y deroga la Ley Nº 29060, Ley del Silencio Administrativo” se modifica e incorporan algunos artículos al dispositivo legal antes citado;

Que, mediante Decreto Supremo Nº 006-2017-JUS, se aprobó el Texto Único Ordenado de la Ley del Procedimiento Administrativo General;

Que, posteriormente, mediante Decreto Legislativo Nº 1452, Decreto Legislativo que modifica la Ley Nº 27444, Ley del Procedimiento Administrativo General; se modifica e incorporan algunos numerales y artículos a la Ley Nº 27444, Ley del Procedimiento Administrativo General;

Que, de acuerdo a la Primera Disposición Complementaria Final del Decreto Legislativo Nº 1452, las entidades del Poder Ejecutivo se encuentran facultadas a compilar en el respectivo Texto Único Ordenado las modificaciones efectuadas a disposiciones legales o reglamentarias de alcance general correspondientes al sector al que pertenecen con la finalidad de compilar toda la normativa en un solo texto; y su aprobación se produce mediante decreto supremo del sector correspondiente, debiendo contar con la opinión previa favorable del Ministerio de Justicia y Derechos Humanos;

Que, la Tercera Disposición Complementaria Final del Decreto Legislativo Nº 1452, dispuso que el Ministerio de Justicia y Derechos Humanos, en un plazo no mayor a 60 (sesenta) días hábiles de publicado, incorpore las modificaciones contenidas en la norma al Texto Único Ordenado de la Ley del Procedimiento Administrativo General, aprobado por Decreto Supremo Nº 006-2017-JUS;

De conformidad con lo dispuesto por el inciso 8) del artículo 118 de la Constitución Política del Perú; en la Ley Nº 29158, Ley Orgánica del Poder Ejecutivo y en el Decreto Legislativo Nº 1452, Decreto Legislativo que modifica la Ley Nº 27444, Ley del Procedimiento Administrativo General.

DECRETA:

Artículo 1.- Aprobación del Texto Único Ordenado de la Ley Nº 27444, Ley del Procedimiento Administrativo General.

Apruébase el Texto Único Ordenado de la Ley Nº 27444, Ley del Procedimiento Administrativo General, que consta de cinco (5) títulos, diecinueve capítulos (19), doscientos sesenta y cinco (265) artículos, diez (10) Disposiciones Complementarias Finales, trece (13) Disposiciones Complementarias Transitorias; y, tres (3) Disposiciones Complementarias Derogatorias.

Artículo 2.- Derogación

Deróguese, a partir de la vigencia de la presente norma, el Texto Único Ordenado de la Ley Nº 27444, Ley del Procedimiento Administrativo General, aprobado por Decreto Supremo Nº 006-2017-JUS.

Artículo 3.- Publicación.

Disponer la publicación del presente Decreto Supremo en el diario oficial El Peruano, en el Portal Institucional del Estado Peruano (www.peru.gob.pe) y en el Portal Institucional del Ministerio de Justicia y Derechos Humanos (www.minjus.gob.pe), el mismo día de la publicación de la presente norma.

Artículo 4.- Refrendo.

El presente Decreto Supremo es refrendado por el Ministro de Justicia y Derechos Humanos.

Dado en la Casa de Gobierno, en Lima, a los veintidós días del mes de enero del año dos mil diecinueve.

MARTÍN ALBERTO VIZCARRA CORNEJO

Presidente de la República

VICENTE ANTONIO ZEBALLOS SALINAS

Ministro de Justicia y Derechos Humanos

TEXTO ÚNICO ORDENADO DE LA LEY DEL PROCEDIMIENTO ADMINISTRATIVO GENERAL

TÍTULO PRELIMINAR

==============
CAPITULO UNICO
==============

Artículo I. Ámbito de aplicación de la ley
==========================================

La presente Ley será de aplicación para todas las entidades de la Administración Pública.

Para los fines de la presente Ley, se entenderá por “entidad” o “entidades” de la Administración Pública:

1. El Poder Ejecutivo, incluyendo Ministerios y Organismos Públicos;

2. El Poder Legislativo;

3. El Poder Judicial;

4. Los Gobiernos Regionales;

5. Los Gobiernos Locales;

6. Los Organismos a los que la Constitución Política del Perú y las leyes confieren autonomía.

7. Las demás entidades, organismos, proyectos especiales, y programas estatales, cuyas actividades se realizan en virtud de potestades administrativas y, por tanto, se consideran sujetas a las normas comunes de derecho público, salvo mandato expreso de ley que las refiera a otro régimen; y,

8. Las personas jurídicas bajo el régimen privado que prestan servicios públicos o ejercen función administrativa, en virtud de concesión, delegación o autorización del Estado, conforme a la normativa de la materia.

Los procedimientos que tramitan las personas jurídicas mencionadas en el párrafo anterior se rigen por lo dispuesto en la presente Ley, en lo que fuera aplicable de acuerdo a su naturaleza privada.

(Texto modificado según el artículo 2 del Decreto Legislativo Nº 1272)

Artículo II.- Contenido
=======================

1. La presente Ley contiene normas comunes para las actuaciones de la función administrativa del Estado y, regula todos los procedimientos administrativos desarrollados en las entidades, incluyendo los procedimientos especiales.

2. Las leyes que crean y regulan los procedimientos especiales no podrán imponer condiciones menos favorables a los administrados que las previstas en la presente Ley.

3. Las autoridades administrativas, al reglamentar los procedimientos especiales, cumplirán con seguir los principios administrativos, así como los derechos y deberes de los sujetos del procedimiento, establecidos en la presente Ley.

(Texto modificado según el artículo 2 del Decreto Legislativo Nº 1272)

Artículo III.- Finalidad
========================

La presente Ley tiene por finalidad establecer el régimen jurídico aplicable para que la actuación de la Administración Pública sirva a la protección del interés general, garantizando los derechos e intereses de los administrados y con sujeción al ordenamiento constitucional y jurídico en general.

(Texto según el artículo III de la Ley Nº 27444)

Artículo IV. Principios del procedimiento administrativo
========================================================

1. El procedimiento administrativo se sustenta fundamentalmente en los siguientes principios, sin perjuicio de la vigencia de otros principios generales del Derecho Administrativo:

1.1. Principio de legalidad.- Las autoridades administrativas deben actuar con respeto a la Constitución, la ley y al derecho, dentro de las facultades que le estén atribuidas y de acuerdo con los fines para los que les fueron conferidas.

1.2. Principio del debido procedimiento.- Los administrados gozan de los derechos y garantías implícitos al debido procedimiento administrativo. Tales derechos y garantías comprenden, de modo enunciativo mas no limitativo, los derechos a ser notificados; a acceder al expediente; a refutar los cargos imputados; a exponer argumentos y a presentar alegatos complementarios; a ofrecer y a producir pruebas; a solicitar el uso de la palabra, cuando corresponda; a obtener una decisión motivada, fundada en derecho, emitida por autoridad competente, y en un plazo razonable; y, a impugnar las decisiones que los afecten.

La institución del debido procedimiento administrativo se rige por los principios del Derecho Administrativo. La regulación propia del Derecho Procesal es aplicable solo en cuanto sea compatible con el régimen administrativo.

1.3. Principio de impulso de oficio.- Las autoridades deben dirigir e impulsar de oficio el procedimiento y ordenar la realización o práctica de los actos que resulten convenientes para el esclarecimiento y resolución de las cuestiones necesarias.

1.4. Principio de razonabilidad.- Las decisiones de la autoridad administrativa, cuando creen obligaciones, califiquen infracciones, impongan sanciones, o establezcan restricciones a los administrados, deben adaptarse dentro de los límites de la facultad atribuida y manteniendo la debida proporción entre los medios a emplear y los fines públicos que deba tutelar, a fin de que respondan a lo estrictamente necesario para la satisfacción de su cometido.

1.5. Principio de imparcialidad.- Las autoridades administrativas actúan sin ninguna clase de discriminación entre los administrados, otorgándoles tratamiento y tutela igualitarios frente al procedimiento, resolviendo conforme al ordenamiento jurídico y con atención al interés general.

1.6. Principio de informalismo.- Las normas de procedimiento deben ser interpretadas en forma favorable a la admisión y decisión final de las pretensiones de los administrados, de modo que sus derechos e intereses no sean afectados por la exigencia de aspectos formales que puedan ser subsanados dentro del procedimiento, siempre que dicha excusa no afecte derechos de terceros o el interés público.

1.7. Principio de presunción de veracidad.- En la tramitación del procedimiento administrativo, se presume que los documentos y declaraciones formulados por los administrados en la forma prescrita por esta Ley, responden a la verdad de los hechos que ellos afirman. Esta presunción admite prueba en contrario.

1.8. Principio de buena fe procedimental.- La autoridad administrativa, los administrados, sus representantes o abogados y, en general, todos los partícipes del procedimiento, realizan sus respectivos actos procedimentales guiados por el respeto mutuo, la colaboración y la buena fe. La autoridad administrativa no puede actuar contra sus propios actos, salvo los supuestos de revisión de oficio contemplados en la presente Ley.

Ninguna regulación del procedimiento administrativo puede interpretarse de modo tal que ampare alguna conducta contra la buena fe procedimental.

1.9. Principio de celeridad.- Quienes participan en el procedimiento deben ajustar su actuación de tal modo que se dote al trámite de la máxima dinámica posible, evitando actuaciones procesales que dificulten su desenvolvimiento o constituyan meros formalismos, a fin de alcanzar una decisión en tiempo razonable, sin que ello releve a las autoridades del respeto al debido procedimiento o vulnere el ordenamiento.

1.10. Principio de eficacia.- Los sujetos del procedimiento administrativo deben hacer prevalecer el cumplimiento de la finalidad del acto procedimental, sobre aquellos formalismos cuya realización no incida en su validez, no determinen aspectos importantes en la decisión final, no disminuyan las garantías del procedimiento, ni causen indefensión a los administrados.

En todos los supuestos de aplicación de este principio, la finalidad del acto que se privilegie sobre las formalidades no esenciales deberá ajustarse al marco normativo aplicable y su validez será una garantía de la finalidad pública que se busca satisfacer con la aplicación de este principio.

1.11. Principio de verdad material.- En el procedimiento, la autoridad administrativa competente deberá verificar plenamente los hechos que sirven de motivo a sus decisiones, para lo cual deberá adoptar todas las medidas probatorias necesarias autorizadas por la ley, aun cuando no hayan sido propuestas por los administrados o hayan acordado eximirse de ellas.

En el caso de procedimientos trilaterales la autoridad administrativa estará facultada a verificar por todos los medios disponibles la verdad de los hechos que le son propuestos por las partes, sin que ello signifique una sustitución del deber probatorio que corresponde a estas. Sin embargo, la autoridad administrativa estará obligada a ejercer dicha facultad cuando su pronunciamiento pudiera involucrar también al interés público.

1.12. Principio de participación.- Las entidades deben brindar las condiciones necesarias a todos los administrados para acceder a la información que administren, sin expresión de causa, salvo aquellas que afectan la intimidad personal, las vinculadas a la seguridad nacional o las que expresamente sean excluidas por ley; y extender las posibilidades de participación de los administrados y de sus representantes, en aquellas decisiones públicas que les puedan afectar, mediante cualquier sistema que permita la difusión, el servicio de acceso a la información y la presentación de opinión.

1.13. Principio de simplicidad.- Los trámites establecidos por la autoridad administrativa deberán ser sencillos, debiendo eliminarse toda complejidad innecesaria; es decir, los requisitos exigidos deberán ser racionales y proporcionales a los fines que se persigue cumplir.

1.14. Principio de uniformidad.- La autoridad administrativa deberá establecer requisitos similares para trámites similares, garantizando que las excepciones a los principios generales no serán convertidos en la regla general. Toda diferenciación deberá basarse en criterios objetivos debidamente sustentados.

1.15. Principio de predictibilidad o de confianza legítima.- La autoridad administrativa brinda a los administrados o sus representantes información veraz, completa y confiable sobre cada procedimiento a su cargo, de modo tal que, en todo momento, el administrado pueda tener una comprensión cierta sobre los requisitos, trámites, duración estimada y resultados posibles que se podrían obtener.

Las actuaciones de la autoridad administrativa son congruentes con las expectativas legítimas de los administrados razonablemente generadas por la práctica y los antecedentes administrativos, salvo que por las razones que se expliciten, por escrito, decida apartarse de ellos.

La autoridad administrativa se somete al ordenamiento jurídico vigente y no puede actuar arbitrariamente. En tal sentido, la autoridad administrativa no puede variar irrazonable e inmotivadamente la interpretación de las normas aplicables.

1.16. Principio de privilegio de controles posteriores.- La tramitación de los procedimientos administrativos se sustentará en la aplicación de la fiscalización posterior; reservándose la autoridad administrativa, el derecho de comprobar la veracidad de la información presentada, el cumplimiento de la normatividad sustantiva y aplicar las sanciones pertinentes en caso que la información presentada no sea veraz.

1.17. Principio del ejercicio legítimo del poder.- La autoridad administrativa ejerce única y exclusivamente las competencias atribuidas para la finalidad prevista en las normas que le otorgan facultades o potestades, evitándose especialmente el abuso del poder, bien sea para objetivos distintos de los establecidos en las disposiciones generales o en contra del interés general.

1.18. Principio de responsabilidad.- La autoridad administrativa está obligada a responder por los daños ocasionados contra los administrados como consecuencia del mal funcionamiento de la actividad administrativa, conforme lo establecido en la presente ley. Las entidades y sus funcionarios o servidores asumen las consecuencias de sus actuaciones de acuerdo con el ordenamiento jurídico.

1.19. Principio de acceso permanente.- La autoridad administrativa está obligada a facilitar información a los administrados que son parte en un procedimiento administrativo tramitado ante ellas, para que en cualquier momento del referido procedimiento puedan conocer su estado de tramitación y a acceder y obtener copias de los documentos contenidos en dicho procedimiento, sin perjuicio del derecho de acceso a la información que se ejerce conforme a la ley de la materia.

2. Los principios señalados servirán también de criterio interpretativo para resolver las cuestiones que puedan suscitarse en la aplicación de las reglas de procedimiento, como parámetros para la generación de otras disposiciones administrativas de carácter general, y para suplir los vacíos en el ordenamiento administrativo.

La relación de principios anteriormente enunciados no tiene carácter taxativo.

(Texto modificado según el artículo 2 del Decreto Legislativo Nº 1272)

Artículo V.- Fuentes del procedimiento administrativo
=====================================================

1. El ordenamiento jurídico administrativo integra un sistema orgánico que tiene autonomía respecto de otras ramas del Derecho.

2. Son fuentes del procedimiento administrativo:

2.1. Las disposiciones constitucionales.

2.2. Los tratados y convenios internacionales incorporados al Ordenamiento Jurídico Nacional.

2.3. Las leyes y disposiciones de jerarquía equivalente.

2.4. Los Decretos Supremos y demás normas reglamentarias de otros poderes del Estado.

2.5. Los demás reglamentos del Poder Ejecutivo, los estatutos y reglamentos de las entidades, así como los de alcance institucional o provenientes de los sistemas administrativos.

2.6. Las demás normas subordinadas a los reglamentos anteriores.

2.7. La jurisprudencia proveniente de las autoridades jurisdiccionales que interpreten disposiciones administrativas.

2.8. Las resoluciones emitidas por la Administración a través de sus tribunales o consejos regidos por leyes especiales, estableciendo criterios interpretativos de alcance general y debidamente publicadas. Estas decisiones generan precedente administrativo, agotan la vía administrativa y no pueden ser anuladas en esa sede.

2.9. Los pronunciamientos vinculantes de aquellas entidades facultadas expresamente para absolver consultas sobre la interpretación de normas administrativas que apliquen en su labor, debidamente difundidas.

2.10. Los principios generales del derecho administrativo.

3. Las fuentes señaladas en los numerales 2.7, 2.8, 2.9 y 2.10 sirven para interpretar y delimitar el campo de aplicación del ordenamiento positivo al cual se refieren.

(Texto según el artículo V de la Ley Nº 27444)

Artículo VI.- Precedentes administrativos
=========================================

1. Los actos administrativos que al resolver casos particulares interpreten de modo expreso y con carácter general el sentido de la legislación, constituirán precedentes administrativos de observancia obligatoria por la entidad, mientras dicha interpretación no sea modificada. Dichos actos serán publicados conforme a las reglas establecidas en la presente norma.

2. Los criterios interpretativos establecidos por las entidades, podrán ser modificados si se considera que no es correcta la interpretación anterior o es contraria al interés general. La nueva interpretación no podrá aplicarse a situaciones anteriores, salvo que fuere más favorable a los administrados.

3. En todo caso, la sola modificación de los criterios no faculta a la revisión de oficio en sede administrativa de los actos firmes.

(Texto según el artículo VI de la Ley Nº 27444)

Artículo VII.- Función de las disposiciones generales
=====================================================

1. Las autoridades superiores pueden dirigir u orientar con carácter general la actividad de los subordinados a ellas mediante circulares, instrucciones y otros análogos, los que sin embargo, no pueden crear obligaciones nuevas a los administrados.

2. Dichas disposiciones deben ser suficientemente difundidas, colocadas en lugar visible de la entidad si su alcance fuera meramente institucional, o publicarse si fuera de índole externa.

3. Los administrados pueden invocar a su favor estas disposiciones, en cuanto establezcan obligaciones a los órganos administrativos en su relación con los administrados.

(Texto según el artículo VII de la Ley Nº 27444)

Artículo VIII.- Deficiencia de fuentes
======================================

1. Las autoridades administrativas no podrán dejar de resolver las cuestiones que se les proponga, por deficiencia de sus fuentes; en tales casos, acudirán a los principios del procedimiento administrativo previstos en esta Ley; en su defecto, a otras fuentes supletorias del derecho administrativo, y sólo subsidiariamente a éstas, a las normas de otros ordenamientos que sean compatibles con su naturaleza y finalidad.

2. Cuando la deficiencia de la normativa lo haga aconsejable, complementariamente a la resolución del caso, la autoridad elaborará y propondrá a quien competa, la emisión de la norma que supere con carácter general esta situación, en el mismo sentido de la resolución dada al asunto sometido a su conocimiento.

(Texto según el artículo VIII de la Ley Nº 27444)

